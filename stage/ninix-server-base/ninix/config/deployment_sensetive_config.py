from ninix.config.general_config_constants import (
    OtherConstants,
    TestTwilioConstants,
    StageTwilioConstants,
    LiveTwilioConstants,
    GeneralConstants,
)


class DevelopmentSensitiveConfigConstants(GeneralConstants,
                                          OtherConstants,
                                          TestTwilioConstants,
                                          ):
    PROXY_MODE = True
    DEBUG_MODE = True
    DEVELOPER_MODE = True
    IGNORE_PERMISSION = True
    IGNORE_JWT_TOKEN = False
    IGNORE_PROTECT_MODE = True


class StageSensitiveConfigConstants(GeneralConstants,
                                    OtherConstants,
                                    StageTwilioConstants,
                                    ):
    PROXY_MODE = True
    #DEBUG_MODE = False
    DEBUG_MODE = True
    DEVELOPER_MODE = True
    IGNORE_PERMISSION = False
    IGNORE_JWT_TOKEN = False
    IGNORE_PROTECT_MODE = True


class LiveSensitiveConfigConstants(GeneralConstants,
                                   OtherConstants,
                                   LiveTwilioConstants,
                                   ):
    PROXY_MODE = True
    DEBUG_MODE = False
    DEVELOPER_MODE = False
    IGNORE_PERMISSION = False
    IGNORE_JWT_TOKEN = False
    IGNORE_PROTECT_MODE = True
