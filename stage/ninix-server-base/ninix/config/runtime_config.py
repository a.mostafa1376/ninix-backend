import os

from ninix.config.deployment_sensetive_config import LiveSensitiveConfigConstants, StageSensitiveConfigConstants, \
    DevelopmentSensitiveConfigConstants


class DefaultRuntimeConfig:
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    PROJECT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    # media directory
    MEDIA_URL = '/media/'
    MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

    STATIC_ROOT = BASE_DIR

    # routing config constants
    APP_FQDN = 'localhost'
    HOST_IP = '127.0.0.1'
    PROTOCOL = 'http://'
    API_PREFIX = 'api/'
    API_VERSION = 'v1/'
    API_ROUTE = API_PREFIX + API_VERSION

    INTERNAL_ADDRESS = PROTOCOL + 'localhost:8000/'
    INTERNAL_API_ADDRESS = INTERNAL_ADDRESS + API_ROUTE
    INTERNAL_IP_ADDRESS = PROTOCOL + '127.0.0.1:8000/'
    INTERNAL_IP_API_ADDRESS = INTERNAL_IP_ADDRESS + API_ROUTE

    EXTERNAL_ADDRESS = PROTOCOL + APP_FQDN + '/'
    EXTERNAL_API_ADDRESS = EXTERNAL_ADDRESS + API_ROUTE
    EXTERNAL_IP_ADDRESS = PROTOCOL + HOST_IP + '/'
    EXTERNAL_IP_API_ADDRESS = EXTERNAL_IP_ADDRESS + API_ROUTE

    # data base config constants
    DB_SCHEMA = None
    DB_TEST_SCHEMA = None
    DB_USERNAME = None
    DB_PASSWORD = None
    DB_URL = None
    DB_PORT = None

    # ELK base config constants
    LOGSTASH_NAME = 'ninix-logstash-log'
    LOGSTASH_HOST = '127.0.0.1'
    LOGSTASH_PORT = 5000
    LOGSTASH_DATABASE = 'logstash.db'

    # Mongodb config constants
    MONGODB_HOST = '127.0.0.1'
    MONGODB_PORT = 27017
    MONGODB_DATABASE = 'ninix-log'
    MONGODB_COLLECTION = 'tracking'
    MONGODB_USERNAME = 'root'
    MONGODB_PASSWORD = '8nuvoXAXw+DM'
    MONGODB_AUTH_MECHANISM = 'SCRAM-SHA-256'

    # minio config constants
    MINIO_HOST = '127.0.0.1:9000'
    MINIO_ACCESS_KEY = 'sssanikhani'
    MINIO_SECRET_KEY = 'sssanikhani'

    # default messaging adapter
    MESSAGE_ADAPTER_CLASS = 'SmsIrMessageAdapter'


class LocalRuntimeConfig(DefaultRuntimeConfig, DevelopmentSensitiveConfigConstants):
    PROTOCOL = 'http://'
    API_PREFIX = 'api/'
    API_VERSION = 'v1/'
    API_ROUTE = API_PREFIX + API_VERSION

    
    DB_SCHEMA = 'ninix_local_0.1.1'
    DB_TEST_SCHEMA = 'ninix_local_0.1.1'
    DB_USERNAME = 'root'
    DB_PASSWORD = '8nuvoXAXw+DM'
    DB_URL = '127.0.0.1'
    DB_PORT = '3306'
    
    


class StageRuntimeConfig(DefaultRuntimeConfig, StageSensitiveConfigConstants):
    APP_FQDN = 'stage.dyonical.ir'
    HOST_IP = '193.176.241.209'

    STATIC_ROOT = '/home/ali/stage/ninix-client-web/'

    PROTOCOL = 'http://'
    API_PREFIX = 'api/'
    API_VERSION = 'v1/'
    API_ROUTE = API_PREFIX + API_VERSION
    EXTERNAL_ADDRESS = PROTOCOL + APP_FQDN + '/'
    EXTERNAL_API_ADDRESS = EXTERNAL_ADDRESS + API_ROUTE
    EXTERNAL_IP_ADDRESS = PROTOCOL + HOST_IP + '/'
    EXTERNAL_IP_API_ADDRESS = EXTERNAL_IP_ADDRESS + API_ROUTE

    MONGODB_DATABASE = 'ninix-log-stage'
    
    
    DB_SCHEMA = 'ninix_staging_0.1.1'
    DB_TEST_SCHEMA = 'ninix_staging_0.1.1'
    
    DB_USERNAME = 'superhero'
    DB_PASSWORD = 'wNSHy5v3vi27oaYlObXNK06L0tF/i+I9itBU'
    DB_URL = 'localhost'
    DB_PORT = '3306'
    
    
#     DB_SCHEMA = 'ninix_staging_0.1.1'
#     DB_TEST_SCHEMA = 'ninix_staging_0.1.1'
#     DB_USERNAME = 'root'
#     DB_PASSWORD = '8nuvoXAXw+DM'
#     DB_URL = 'localhost'
#     DB_PORT = '3306'


class LiveRuntimeConfig(DefaultRuntimeConfig, LiveSensitiveConfigConstants):
    APP_FQDN = 'web.dyonical.ir'
    HOST_IP = '193.176.241.209'

    STATIC_ROOT = '/opt/live/ninix-client-web/'

    PROTOCOL = 'http://'
    API_PREFIX = 'api/'
    API_VERSION = 'v1/'
    API_ROUTE = API_PREFIX + API_VERSION

    EXTERNAL_ADDRESS = PROTOCOL + APP_FQDN + '/'
    EXTERNAL_API_ADDRESS = EXTERNAL_ADDRESS + API_ROUTE
    EXTERNAL_IP_ADDRESS = PROTOCOL + HOST_IP + '/'
    EXTERNAL_IP_API_ADDRESS = EXTERNAL_IP_ADDRESS + API_ROUTE

    MONGODB_DATABASE = 'ninix-log-live'

    DB_SCHEMA = 'ninix_live_1.0.1'
    DB_TEST_SCHEMA = 'ninix_staging_0.1.1'
    DB_USERNAME = 'superhero'
    DB_PASSWORD = 'wNSHy5v3vi27oaYlObXNK06L0tF/i+I9itBU'
    DB_URL = 'localhost'
    DB_PORT = '3306'


class RuntimeConfig(StageRuntimeConfig):
    pass
