from ninix.constants.messages.en.authorization_messages import AuthorizationMessages


class EnMessages(AuthorizationMessages):
    MESSAGE_JOIN_CHAR = ', '
    MESSAGE_LINE_BREAK_CHAR = '\n'
    MESSAGE_SIGNATURE = 'Ninix'
