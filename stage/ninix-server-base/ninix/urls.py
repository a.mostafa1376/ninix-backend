"""ninix URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from nnx.controllers.device.boot_loader_view import boot_loader_upload_view
from nnx.controllers.device.firmware_view import firmware_upload_view
from nnx.controllers.device.hub_firmware_view import hub_firmware_upload_view

# from rest_framework import routers
# from nnx import views

# router = routers.DefaultRouter()
# router.register(r'users', views.UserViewSet)
# router.register(r'groups', views.GroupViewSet)


prefix = "api/v1/"

urlpatterns = [
#     path('', include(router.urls)),
    path('boot-loader-upload/', boot_loader_upload_view),
    path('firmware-upload/', firmware_upload_view),
    path('hub-firmware-upload/', hub_firmware_upload_view),
    
    path(prefix, include('nnx.urls')),
    path('cms/', admin.site.urls),
    path('auth/', include('rest_framework_social_oauth2.urls')),
]
