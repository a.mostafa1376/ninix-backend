from django.urls import re_path

from nnx.consumers.ninix_data_consumer import NinixDataConsumer

websocket_urlpatterns = [
    re_path(r'ws/rtd/(?P<serial_number>\d+)/$', NinixDataConsumer),
]
