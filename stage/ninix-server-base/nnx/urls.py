from django.urls import path

from nnx.controllers.authorization.check_passcode_view import CheckPasscode
from nnx.controllers.authorization.generate_passcode_view import GeneratePasscode
from nnx.controllers.authorization.kid_view import KidView
from nnx.controllers.authorization.member_view import MemberView
from nnx.controllers.device.boot_loader_view import BootLoaderView
from nnx.controllers.device.device_parameter_view import DeviceParameterView
from nnx.controllers.device.device_view import DeviceView
from nnx.controllers.device.firmware_view import FirmwareView
from nnx.controllers.device.hub_firmware_view import HubFirmwareView
from nnx.controllers.device.notification_view import NotificationView
from nnx.controllers.log.raw_log_view import RawLogView
from nnx.controllers.log.tracking_log_view import TrackingLogView
from nnx.controllers.log.active_devices_view import ActiveDevicesView
from nnx.controllers.log.member_logs_view import MemberLogsView
from nnx.controllers.media.kid_image_view import KidImageView
from nnx.controllers.media.member_image_view import MemberImageView

# authorization routes ...
authorization_urlpatterns = [
    # GET,
    path('generate-passcode/', GeneratePasscode.as_view(), name='generate passcode'),

    # POST,
    path('check-passcode/', CheckPasscode.as_view(), name='check passcode'),

    # GET, POST,
    path('member/', MemberView.as_view(), name='member info'),

    # GET, POST,
    path('kid/', KidView.as_view(), name='kid info'),

    # GET, POST,
    path('kid/images/', KidImageView.as_view(), name='kid image'),

    # GET, POST,
    path('member/images/', MemberImageView.as_view(), name='member image'),
]

# device routes ...
device_urlpatterns = [
    # GET, POST,
    path('boot-loader/', BootLoaderView.as_view(), name='boot-loader info'),

    # GET,
    path('device-parameters/', DeviceParameterView.as_view(), name='device parameters info'),

    # GET, POST,
    path('firmware/', FirmwareView.as_view(), name='firmware info'),
    
    path('hub-firmware/', HubFirmwareView.as_view(), name='hub firmware info'),

    # POST,
    path('stock/serial', DeviceView.as_view(), name='device info'),

    # GET, POST,
    path('data/notification/', NotificationView.as_view(), name='notification'),

]

# logging routes ...
logging_urlpatterns = [
    # POST,
    path('log/raw/', RawLogView.as_view(), name='log raw'),

    # POST,
    path('log/tracking/', TrackingLogView.as_view(), name='log tracking'),
    
    # POST,
    path('log/active-devices/', ActiveDevicesView.as_view(), name='active devices'),
    
    # GET,
    path('log/member-logs/', MemberLogsView.as_view(), name='member logs'),
    
]

urlpatterns = [
    *authorization_urlpatterns,
    *device_urlpatterns,
    *logging_urlpatterns,
]
