from hs_infra.dal.base.base_dal import BaseDal
from hs_infra.meta_classes.singleton_meta_class import Singleton

from nnx.entities.media.image_file_model import ImageFile


class ImageFileDal(BaseDal, metaclass=Singleton):
    model = ImageFile

    def create_image_file(self, file_path, file_name, image_profile, ext, width, height, aspect_ratio) -> ImageFile:
        image_file_object = self.model.objects.create(
            file=file_path,
            file_name=file_name,
            image_profile=image_profile,
            ext=ext,
            width=width,
            height=height,
            aspect_ratio=aspect_ratio
        )
        return image_file_object

    def get_image_file_by_id(self, object_id) -> ImageFile:
        image_file_qs = self.model.objects.filter(id=object_id)
        if image_file_qs.exists():
            image_file = image_file_qs[0]
        else:
            image_file = None
        return image_file
