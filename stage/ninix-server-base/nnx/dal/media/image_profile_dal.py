from hs_infra.dal.base.base_dal import BaseDal
from hs_infra.meta_classes.singleton_meta_class import Singleton

from nnx.entities.media.image_profile_model import ImageProfile


class ImageProfileDal(BaseDal, metaclass=Singleton):
    model = ImageProfile

    def get_image_profile_by_id(self, object_id):
        image_profile_qs = self.model.objects.filter(id=object_id)
        if image_profile_qs.exists():
            image_profile = image_profile_qs[0]
        else:
            image_profile = None
        return image_profile

    def get_by_address(self, address) -> ImageProfile:
        image_qs = self.model.objects.filter(file=address)
        return image_qs[0]

    def create_image_profile(self, file, file_name, content_object, ext):
        new_object = self.model.objects.create(
            file=file,
            file_name=file_name,
            content_object=content_object,
            extension=ext
        )
        return new_object
