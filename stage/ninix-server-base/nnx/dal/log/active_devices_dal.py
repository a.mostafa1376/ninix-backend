import json
from typing import Dict

from hs_infra.dal.base.base_dal import BaseDal
from hs_infra.meta_classes.singleton_meta_class import Singleton

from nnx.adapters.mongodb_adapter.base_mongodb_dapter import BaseMongodbAdapter

class ActiveDevicesDal(BaseDal, metaclass=Singleton):
    
    
    def find_active_devices(self, from_timestamp, until_timestamp, collection=None):
        mongodb_adapter = BaseMongodbAdapter()    
        query = {'timestamp': {'$gt':from_timestamp, '$lt': until_timestamp}}
        select = {'kid_id':1, '_id':0}
        logs = mongodb_adapter.find_all(query, select, collection)
        kid_ids = []
        for log in logs:
            kid_ids.append(log['kid_id'])
        return set(kid_ids)