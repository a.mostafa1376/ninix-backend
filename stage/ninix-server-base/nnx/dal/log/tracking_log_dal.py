import json
from typing import Dict

from hs_infra.dal.base.base_dal import BaseDal
from hs_infra.meta_classes.singleton_meta_class import Singleton
from hs_infra.utils.utils import Utils

from nnx.adapters.mongodb_adapter.base_mongodb_dapter import BaseMongodbAdapter
from nnx.entities.log.raw_log_model import RawLog
from nnx.entities.log.tracking_log_model import TrackingLog


class TrackingLogDal(BaseDal, metaclass=Singleton):
    model = TrackingLog

    def find_log(self, kid_id, data, collection=None) -> Dict:
        from ast import literal_eval
        mongodb_adapter = BaseMongodbAdapter()
        
        query = {"$and": [{'kid_id': int(kid_id)}, {'timestamp': {'$gt':int(data.get('from')), '$lt': int(data.get('until'))}}]}
        logs = mongodb_adapter.find_all(query, collection)
        columns = literal_eval(data.get('identifier'))
        
        #to_edit
        track_log = []
        for log in logs:
            found_columns = log.keys()
            response = dict()
            for column in columns:
                if column in found_columns:
                    response[column] = log[column]
            if len(response) == 0:
                continue
            response['timestamp'] = log['timestamp']
            track_log.append(response)
            
        
        return track_log



    def create_new_log_record(self, kid_id, overwrite, collection=None, **kwargs) -> RawLog:
        tracking_log = self.create_new(**kwargs)
        mongodb_adapter = BaseMongodbAdapter()
        log = json.loads(tracking_log.log_data)
        mongo_log = dict()
        
        timestamp = tracking_log.timestamp
        mongo_log['kid_id'] = kid_id
        mongo_log['timestamp'] = int(timestamp)
        for key, value in log.items():
            mongo_log[key] = value

        #overwrite = False and same timestamp
        if overwrite == 'True':
            query = {'timestamp': int(timestamp)}
            mongodb_adapter.update(query, mongo_log, collection)
        else:
            mongodb_adapter.insert_one(mongo_log, collection)
        return tracking_log
