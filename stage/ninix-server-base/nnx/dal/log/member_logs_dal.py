import json
from typing import Dict

from hs_infra.dal.base.base_dal import BaseDal
from hs_infra.meta_classes.singleton_meta_class import Singleton

from nnx.adapters.mongodb_adapter.base_mongodb_dapter import BaseMongodbAdapter

class MemberLogsDal(BaseDal, metaclass=Singleton):
    
    
    def find_logs(self, kid_id):
        mongodb_adapter = BaseMongodbAdapter()    
        query = {'kid_id': kid_id}
        select = {'kid_id':0, '_id':0}
        logs = mongodb_adapter.find_all(query, select)
        response = []
        for log in logs:
            response.append(log)
        return response