import json

from hs_infra.dal.base.base_dal import BaseDal
from hs_infra.meta_classes.singleton_meta_class import Singleton
from hs_infra.utils.utils import Utils

from nnx.adapters.mongodb_adapter.base_mongodb_dapter import BaseMongodbAdapter

from nnx.entities.log.raw_log_model import RawLog


class RawLogDal(BaseDal, metaclass=Singleton):
    model = RawLog

    def create_new_log_record(self, kid_id, **kwargs) -> RawLog:
        raw_log = self.create_new(**kwargs)
        mongodb_adapter = BaseMongodbAdapter()
        log = json.loads(raw_log.log_data)
        mongo_log = dict()
        for key, value in log.items():
            timestamp = int(Utils.get_current_datetime().timestamp())
            timestamp_key = f'{key}.{timestamp}'
            mongo_log[timestamp_key] = value
        query = {'meta_data.kid_id': int(kid_id)}
        mongodb_adapter.update(query, mongo_log)
        # todo: problem found when used on server
        # logstash_adapter = LogstashAdapter()
        # logstash_adapter.log(
        #     message=raw_log.log_message,
        #     data=json.loads(raw_log.log_data),
        #     level=raw_log.log_level,
        # )
        return raw_log