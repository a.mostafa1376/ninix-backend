from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from hs_infra.dal.base.base_dal import BaseDal
from hs_infra.meta_classes.singleton_meta_class import Singleton

from ninix.config.runtime_config import RuntimeConfig
from nnx.entities.authorization.member_model import Member
from nnx.enums.authorization.active_status_type import ActiveStatusType


class MemberDal(BaseDal, metaclass=Singleton):
    model = Member

    def find_member_by_mobile_number(self, mobile_number: str):
        filters_dict = {
            self.model.mobile_number.field_name: mobile_number
        }
        qs = self.model.objects.filter(
            **filters_dict,
        )
        return qs

    def create_new_member(self, mobile_number, first_name, last_name, relationship_type, birth_year,
                          password=None) -> Member:
        if password is None:
            password = RuntimeConfig.DEFAULT_PASSWORD
        user = self.get_or_create_user(mobile_number, password)
        member = self.model.objects.create(
            mobile_number=mobile_number,
            first_name=first_name,
            last_name=last_name,
            birth_year=birth_year,
            active_status_type=ActiveStatusType.active.value,
            relationship_type=relationship_type.value,
            user=user,
        )
        return member

    @staticmethod
    def get_or_create_user(mobile_number, password=None) -> User:
        if password is None:
            password = RuntimeConfig.DEFAULT_PASSWORD
        user_qs = User.objects.filter(username=mobile_number)
        if user_qs.exists():
            user = user_qs[0]
        else:
            user = User.objects.create(
                username=mobile_number,
                password=make_password(password),
            )
        return user
