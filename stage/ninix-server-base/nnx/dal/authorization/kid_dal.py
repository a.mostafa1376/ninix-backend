from hs_infra.dal.base.base_dal import BaseDal
from hs_infra.meta_classes.singleton_meta_class import Singleton

from nnx.entities.authorization.kid_model import Kid


class KidDal(BaseDal, metaclass=Singleton):
    model = Kid

    def create_new_kid(self, first_name, last_name, gender, birth_date, device_uid) -> Kid:
        kid = self.model.objects.create(
            first_name=first_name,
            last_name=last_name,
            gender=gender,
            birth_date=birth_date,
            device_uid=device_uid,
        )
        return kid

    def get_by_device_uid(self, device_uid) -> Kid:
        kid_qs = self.model.objects.filter(device_uid=device_uid)
        kid = None
        if kid_qs.exists():
            kid = kid_qs[0]
        return kid

    

    def get_by_kid_id(self, kid_id) -> Kid:
        kid_qs = self.model.objects.filter(id=kid_id)
        kid = None
        if kid_qs.exists():
            kid = kid_qs[0]
        return kid
