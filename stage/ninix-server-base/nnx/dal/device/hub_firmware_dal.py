from hs_infra.dal.base.base_dal import BaseDal
from hs_infra.meta_classes.singleton_meta_class import Singleton

from nnx.entities.device.hub_firmware_model import HubFirmware


class HubFirmwareDal(BaseDal, metaclass=Singleton):
    model = HubFirmware

    def get_by_version_code(self, version_code):
        firmware_qs = self.model.objects.filter(
            version_code__gt=version_code
        ).order_by('-version_code')
        return firmware_qs

    def create_new_hub_firmware(self, version_no, version_code, change_log, hub_firmware_src):
        new_hub_firmware = None
        firmware_exist = self.model.objects.filter(version_code__gte=version_code).exists()
        if not firmware_exist:
            criteria = {
                self.model.version_no.field_name: version_no,
                self.model.version_code.field_name: version_code,
                self.model.change_log.field_name: change_log,
                self.model.hub_firmware_src.field_name: hub_firmware_src,
            }
            new_hub_firmware = self.create_new(**criteria)
        return new_hub_firmware
