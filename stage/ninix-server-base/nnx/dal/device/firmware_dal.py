from hs_infra.dal.base.base_dal import BaseDal
from hs_infra.meta_classes.singleton_meta_class import Singleton

from nnx.entities.device.firmware_model import Firmware


class FirmwareDal(BaseDal, metaclass=Singleton):
    model = Firmware

    def get_by_version_code(self, version_code):
        firmware_qs = self.model.objects.filter(
            version_code__gt=version_code
        ).order_by('-version_code')
        return firmware_qs

    def create_new_firmware(self, version_no, version_code, change_log, firmware_src):
        new_firmware = None
        firmware_exist = self.model.objects.filter(version_code__gte=version_code).exists()
        if not firmware_exist:
            criteria = {
                self.model.version_no.field_name: version_no,
                self.model.version_code.field_name: version_code,
                self.model.change_log.field_name: change_log,
                self.model.firmware_src.field_name: firmware_src,
            }
            new_firmware = self.create_new(**criteria)
        return new_firmware
