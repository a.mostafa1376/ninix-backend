from typing import List

from hs_infra.dal.base.base_dal import BaseDal
from hs_infra.meta_classes.singleton_meta_class import Singleton

from nnx.entities.device.device_parameter_model import DeviceParameter


class DeviceParameterDal(BaseDal, metaclass=Singleton):
    model = DeviceParameter

    def get_all_parameters(self, **kwargs) -> List[DeviceParameter]:
        qs = self.model.objects.all()
        parameter_list = list(qs)
        return parameter_list
