from hs_infra.dal.base.base_dal import BaseDal
from hs_infra.meta_classes.singleton_meta_class import Singleton

from nnx.entities.device.notification_model import Notification


class NotificationDal(BaseDal, metaclass=Singleton):
    model = Notification

    def create_new_notification(self, **kwargs) -> Notification:
        return self.create_new(**kwargs)

    def get_last_notification(self, serial_number=None):
        criteria = dict()
        if serial_number:
            criteria[self.model.serial_number.field_name] = serial_number
        notification_qs = self.model.objects.filter(
            **criteria
        ).order_by('-id')
        return notification_qs
