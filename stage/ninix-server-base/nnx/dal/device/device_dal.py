from hs_infra.dal.base.base_dal import BaseDal
from hs_infra.meta_classes.singleton_meta_class import Singleton

from nnx.entities.device.device_model import Device


class DeviceDal(BaseDal, metaclass=Singleton):
    model = Device

    def create_new_device(self, **kwargs) -> Device:
        index = 1
        device_qs = self.model.objects.filter().order_by('-id')[:1]
        if device_qs.exists():
            latest_device = device_qs[0]
            index += latest_device.issue_number
        criteria = {
            self.model.issue_number.field_name: index,
        }
        return self.create_new(**criteria)
