import os

from PIL import Image
from hs_infra.adapters.base.base_adapter import BaseAdapter
from hs_infra.meta_classes.singleton_meta_class import Singleton


class ImageMagickAdapter(BaseAdapter, metaclass=Singleton):
    COMMAND = 'convert'
    IMAGE_OPERATOR_RESIZE = '-resize'
    IMAGE_OPERATOR_CROP = '-crop'
    IMAGE_OPERATOR_GRAVITY = '-gravity'
    IMAGE_GRAVITY_CENTER = 'center'
    X_STR = 'x'
    RATIO_1_1 = 1
    RATIO_16_9 = 16 / 9
    RATIO_4_3 = 4 / 3
    RATIO_3_2 = 3 / 2

    def absolute_resize_image(self, input_file: str, output_file: str, width=100, height=100, keep_aspect_ratio=True):
        """
        :param keep_aspect_ratio: do resizing with keeping aspect-ratio or not.
        :param input_file: absolute path of input image file.
        :param output_file: absolute path of output image file.
        :param width: width of output image file.
        :param height: height of output image file.
        :return:
        """
        directory = os.path.split(output_file)[0]
        if not os.path.exists(directory):
            os.makedirs(directory)

        output_image_size = str(width) + self.X_STR + str(height)
        if not keep_aspect_ratio:
            output_image_size += '!'
        argument_list = [
            self.COMMAND,
            input_file,
            self.IMAGE_OPERATOR_RESIZE,
            output_image_size,
            output_file,
        ]
        prompt = ' '.join(argument_list)
        return os.system(prompt)

    def minimal_crop_image(self, input_file, output_file, aspect_ratio=1):
        """
        :param input_file: absolute path of input image file.
        :param output_file: absolute path of output image file.
        :param aspect_ratio: aspect ratio of output image file.
        :return
        """
        directory = os.path.split(output_file)[0]
        if not os.path.exists(directory):
            os.makedirs(directory)

        image = Image.open(input_file)
        width, height = image.size
        image.close()
        file_ratio = width / height

        common_aspect_ratios = [
            self.RATIO_1_1,
            self.RATIO_16_9,
            self.RATIO_4_3,
            self.RATIO_3_2,
        ]

        if aspect_ratio not in common_aspect_ratios:
            aspect_ratio = self.RATIO_1_1

        if file_ratio > aspect_ratio:
            height_crop = height
            width_crop = int(aspect_ratio * height_crop)
            x_crop = int((width / 2) - (width_crop / 2))
            y_crop = 0
        else:
            width_crop = width
            height_crop = int(width_crop / aspect_ratio)
            x_crop = 0
            y_crop = int((height / 2) - (height_crop / 2))

        output_width, output_height = self._common_size(aspect_ratio)

        crop_size = str(width_crop) + self.X_STR + str(height_crop) + '+{0}+{1}'.format(x_crop, y_crop)
        argument_list = [
            self.COMMAND,
            input_file,
            self.IMAGE_OPERATOR_CROP,
            crop_size,
            self.IMAGE_OPERATOR_GRAVITY,
            self.IMAGE_GRAVITY_CENTER,
            output_file
        ]
        if width_crop > output_width:
            output_image_size = str(output_width) + self.X_STR + str(output_height)
            argument_list.insert(6, output_image_size)
            argument_list.insert(6, self.IMAGE_OPERATOR_RESIZE)

        prompt = ' '.join(argument_list)
        return os.system(prompt)

    def _common_size(self, aspect_ratio):
        if aspect_ratio == self.RATIO_1_1:
            output_width = 1080
            output_height = 1080
        elif aspect_ratio == self.RATIO_16_9:
            output_width = 1920
            output_height = 1080
        elif aspect_ratio == self.RATIO_4_3:
            output_width = 1024
            output_height = 768
        elif aspect_ratio == self.RATIO_3_2:
            output_width = 1080
            output_height = 720
        else:
            output_width = 1080
            output_height = 1080

        return output_width, output_height
