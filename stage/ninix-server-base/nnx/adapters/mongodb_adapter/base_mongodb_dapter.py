from typing import Tuple, Dict

from hs_infra.adapters.base.base_adapter import BaseAdapter
from hs_infra.meta_classes.singleton_meta_class import Singleton
from pymongo import MongoClient

from ninix.config.runtime_config import RuntimeConfig


class BaseMongodbAdapter(BaseAdapter, metaclass=Singleton):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.mongo_client = MongoClient(
            host=RuntimeConfig.MONGODB_HOST,
            port=RuntimeConfig.MONGODB_PORT,
            username=RuntimeConfig.MONGODB_USERNAME,
            password=RuntimeConfig.MONGODB_PASSWORD,
            authMechanism=RuntimeConfig.MONGODB_AUTH_MECHANISM,
        )
        self.default_database = self.mongo_client[RuntimeConfig.MONGODB_DATABASE]
        self.default_collection = RuntimeConfig.MONGODB_COLLECTION

    def insert_one(self, data: Dict, collection: str = None):
        if collection is None:
            collection = self.default_collection
        document = self.default_database[collection].insert_one(data)
        return document

    def pre_aggregate(self, update_fields: Tuple, query_dict: Dict = None, collection=None) -> None:
        if collection is None:
            collection = self.default_collection
        query = dict()
        if query_dict is not None:
            for key, value in query_dict.items():
                query['meta_data.{}'.format(key)] = value
        update = {'$inc': update_fields}
        response = self.default_database[collection].update(query, update, upsert=True)
        return response

    def update(self, query, update_dict: Dict, collection=None):
        if collection is None:
            collection = self.default_collection
        update = {'$set': update_dict}
        response = self.default_database[collection].find_and_modify(query=query, update=update, upsert=True)
        return response

    def find(self, query, collection=None):
        if collection is None:
            collection = self.default_collection
        response = self.default_database[collection].find_one(query)
        return response
    
    def find_all(self, query, select=None, collection=None):
        if collection is None:
            collection = self.default_collection
        response = self.default_database[collection].find(query, select)
        return response


if __name__ == '__main__':
    adapter = BaseMongodbAdapter()
    query_ = {"meta_data": {"kid": 1}}
    a1 = {'weight.1586003113': 20, 'height.1586003113': 92}
    a2 = {'timestamp': '1586008216', 'weight': 20, 'height': 99}
    query = {'meta_data.kid_id': int(152)}
    collection = 'temp'
    adapter.update(query, a1,collection)
    track_log = adapter.find(query, collection)
