import requests
from hs_infra.adapters.message_adapters.base_message_adapter import BaseMessageAdapter
from hs_infra.meta_classes.singleton_meta_class import Singleton
from hs_infra.wrappers.request_wrapper import request_wrapper
from twilio.rest import Client

from ninix.config.runtime_config import RuntimeConfig


class TwilioMessageAdapter(BaseMessageAdapter, metaclass=Singleton):
    """docs: https://www.twilio.com/docs/sms"""

    def __init__(self):
        super().__init__()
        self.account_sid = RuntimeConfig.TWILIO_ACCOUNT_SID
        self.auth_token = RuntimeConfig.TWILIO_AUTH_TOKEN
        self.line_number = RuntimeConfig.TWILIO_LINE_NUMBER

    def push_to_queue(self, message: str, receptor: str):
        if receptor.startswith('+'):
            pass
        else:
            receptor = '+' + receptor
        data = {
            'body': message,
            'from_': self.line_number,
            'to': receptor,
        }
        return self._send_message(data)

    def _send_message(self, data):
        if RuntimeConfig.PROXY_MODE:
            data.update({
                'hey': '45185cb5abbc30d7257104c434fe0b07e5a195a6847506c074527aa599ec'
            })
            response = requests.post(
                url='http://tw.ip.inbeet.tech/twilio/',
                json=data,
            )
        else:
            client = Client(self.account_sid, self.auth_token)
            message = client.messages.create(**data)
            response = message.sid
        return response

    @request_wrapper
    def add_to_contact_list(self, receptor: str):
        """Don't need!"""
        pass


if __name__ == '__main__':
    manager = TwilioMessageAdapter()
    r1 = '+989013330799'
    # r2 = '09017069914'
    # r3 = '09101691879'
    # r4 = '09122348799'
    # m = 'از طرف احسان!'
    m = 'از طرف احسان!'
    print(manager.push_to_queue(m, r1))
    # print(managers.push_to_queue(m, r2))
    # print(managers.push_to_queue(m, r3))
    # print(managers.push_to_queue(m, r4))
    print(manager.send_queued_message())
