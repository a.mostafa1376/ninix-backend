import json

import requests
from hs_infra.adapters.message_adapters.base_message_adapter import BaseMessageAdapter
from hs_infra.meta_classes.singleton_meta_class import Singleton
from hs_infra.wrappers.request_wrapper import request_wrapper


class SmsMessageAdapter(BaseMessageAdapter, metaclass=Singleton):
    """docs: https://web.sms.ir/files/api/PayamakSefid-API.pdf"""
    # todo: Check contact exists, if not create it with api.

    # API_VALUE = '1a9cce495a1a88f74033892d'  # white sms
    API_VALUE = '61ffda4045f4324d73f3de13'  # bulk sms
    SECRET_VALUE = ')&ReKL#*VQ&oq^^'

    API_KEY = 'UserApiKey'
    SECRET_KEY = 'SecretKey'
    TOKEN_KEY = 'TokenKey'

    URL_GET_TOKEN = 'https://RestfulSms.com/api/Token'
    # URL_GET_TOKEN = 'https://api.sms.ir/users/v1/Token/GetToken'
    URL_SEND_MESSAGE = 'https://RestfulSms.com/api/UltraFastSend'
    # URL_SEND_MESSAGE = 'https://api.sms.ir/users/v1/Message/SendByMobileNumbers'
    URL_ADD_CONTACTS = 'https://api.sms.ir/users/v1/Contacts/AddContacts'

    MESSAGE_KEY = 'Message'  # message that will be sent
    RECEPTOR_KEY = 'MobileNumbers'  # a list of mobile numbers
    ERROR_KEY = 'CanContinueInCaseOfError'  # continue or not in case of error

    MOBILE_KEY = 'Mobile'
    TEMPLATE_ID_KEY = 'TemplateId'
    TEMPLATE_ID_VALUE = '8992'
    # 8896, 8987, 8990, 8992, 8993
    PARAMETER_KEY = 'Parameter'
    PARAMETER_VALUE_KEY = 'ParameterValue'
    PARAMETER_ARRAY_KEY = 'ParameterArray'

    HEADER_KEY = 'x-sms-ir-secure-token'  # token of
    CONTENT_TYPE_KEY = 'Content-Type'
    # CONTENT_TYPE_VALUE = 'application/json-patch+json'  # white sms
    CONTENT_TYPE_VALUE = 'application/json'  # bulk sms

    SMS_IR_DEFAULT_GROUP_ID = 8896

    def __init__(self):
        super().__init__()

    def _get_token(self):
        resp, ok, *_ = self._request_for_token()
        if ok:
            token = resp.get(self.TOKEN_KEY)
        else:
            token = None
        return token

    @request_wrapper
    def _request_for_token(self):
        json_data = {
            self.API_KEY: self.API_VALUE,
            self.SECRET_KEY: self.SECRET_VALUE,
        }
        headers = {
            self.CONTENT_TYPE_KEY: self.CONTENT_TYPE_VALUE
        }
        resp = requests.post(
            url=self.URL_GET_TOKEN,
            json=json_data,
            headers=headers,
        )
        return resp

    def push_to_queue(self, message: str, receptor: str, is_verification=False):
        if is_verification:
            data = {
                self.MOBILE_KEY: receptor,
                self.TEMPLATE_ID_KEY: self.TEMPLATE_ID_VALUE,
                self.PARAMETER_ARRAY_KEY: [{
                    self.PARAMETER_KEY: "VerificationCode",
                    self.PARAMETER_VALUE_KEY: message,
                }],
            }
        else:
            data = {
                self.MESSAGE_KEY: message,
                self.RECEPTOR_KEY: [receptor, ],
                self.ERROR_KEY: True,
            }

        value = json.dumps(data)
        return self.redis_manager.push_to_queue(value)

    @request_wrapper
    def _send_message(self, value):
        json_data = json.loads(value)
        token = self._get_token()
        headers = {
            self.HEADER_KEY: token,
            self.CONTENT_TYPE_KEY: self.CONTENT_TYPE_VALUE,
        }
        resp = requests.post(
            url=self.URL_SEND_MESSAGE,
            json=json_data,
            headers=headers,
        )
        return resp

    @request_wrapper
    def add_to_contact_list(self, receptor: str):
        token = self._get_token()
        json_data = {
            'ContactsDetails': [
                {
                    "Mobile": receptor
                },
            ],
            "GroupId": self.SMS_IR_DEFAULT_GROUP_ID,
        }
        headers = {
            self.HEADER_KEY: token,
            self.CONTENT_TYPE_KEY: self.CONTENT_TYPE_VALUE,
        }
        resp = requests.post(
            url=self.URL_ADD_CONTACTS,
            json=json_data,
            headers=headers,
        )
        return resp


if __name__ == '__main__':
    manager = SmsMessageAdapter()
    r1 = '09013330799'
    # r2 = '09017069914'
    # r3 = '09101691879'
    # r4 = '09122348799'
    # m = 'از طرف احسان!'
    m = 'از طرف احسان!'
    print(manager.push_to_queue(m, r1, True))
    # print(managers.push_to_queue(m, r2))
    # print(managers.push_to_queue(m, r3))
    # print(managers.push_to_queue(m, r4))
    print(manager.send_queued_message())
