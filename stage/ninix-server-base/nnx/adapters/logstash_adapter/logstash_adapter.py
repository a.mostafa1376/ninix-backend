import logging

from hs_infra.adapters.base.base_adapter import BaseAdapter
from hs_infra.meta_classes.singleton_meta_class import Singleton
from logstash_async.handler import AsynchronousLogstashHandler
from typing import Dict

from ninix.config.runtime_config import RuntimeConfig
from nnx.enums.log.log_level_type import LogLevelType


class LogstashAdapter(BaseAdapter, metaclass=Singleton):
    """ Docs available at:
        https://python-logstash-async.readthedocs.io/en/latest/usage.html
    Following tool used as pair:
        https://github.com/deviantony/docker-elk
    For raw install need x-pack plugin on ech ELK pack.

    Example extra dict:
        extra = {
            'test_string': 'python version: ' + repr(sys.version_info),
            'test_boolean': True,
            'test_dict': {'a': 1, 'b': 'c'},
            'test_float': 1.23,
            'test_integer': 123,
            'test_list': [1, 2, '3'],
        }
    WARNING: Don't use following keys on extra dict:
        'func_name'
        'interpreter'
        'interpreter_version'
        'line'
        'logger_name'
        'logstash_async_version'
        'path'
        'process_name'
        'thread_name'
    """
    main_logger = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.main_logger = logging.getLogger(RuntimeConfig.LOGSTASH_NAME)
        self.main_logger.setLevel(logging.INFO)
        self.main_logger.addHandler(
            AsynchronousLogstashHandler(
                host=RuntimeConfig.LOGSTASH_HOST,
                port=RuntimeConfig.LOGSTASH_PORT,
                database_path=RuntimeConfig.LOGSTASH_DATABASE,
            )
        )

    def log(self, message, data: Dict = None, level: LogLevelType = None):
        if isinstance(level, str):
            level = LogLevelType.get_type_by_name(level)
        elif isinstance(level, int):
            level = LogLevelType.get_type_by_value(level)
        else:
            level = None
        if not isinstance(level, LogLevelType):
            level = LogLevelType.info

        response = None
        if level is LogLevelType.debug:
            response = self.debug(message, data)
        elif level is LogLevelType.info:
            response = self.info(message, data)
        elif level is LogLevelType.warning:
            response = self.warning(message, data)
        elif level is LogLevelType.error:
            response = self.error(message, data)
        return response

    def debug(self, message, data):
        return self.main_logger.debug(message, extra=data)

    def info(self, message, data):
        return self.main_logger.info(message, extra=data)

    def warning(self, message, data):
        return self.main_logger.warning(message, extra=data)

    def error(self, message, data):
        return self.main_logger.error(message, extra=data)


if __name__ == '__main__':
    adapter = LogstashAdapter()
    from datetime import datetime
    import random

    for i in range(1000):
        data_ = {
            'number': random.randint(1, 10),
            'time': datetime.now(),
        }
        message_ = f'{RuntimeConfig.LOGSTASH_NAME}: info'
        adapter.log(message_, data_)

    input('hello, world?')
