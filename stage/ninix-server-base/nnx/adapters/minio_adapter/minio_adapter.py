from datetime import timedelta

from hs_infra.meta_classes.singleton_meta_class import Singleton
import minio

from ninix.config.runtime_config import RuntimeConfig


class MinioAdapter(metaclass=Singleton):
    CONNECTION_PROTOCOL = "http://"

    def __init__(self):
        self.connection = self._get_connection()

    @staticmethod
    def _get_connection():
        return minio.Minio(
            RuntimeConfig.MINIO_HOST,
            access_key=RuntimeConfig.MINIO_ACCESS_KEY,
            secret_key=RuntimeConfig.MINIO_SECRET_KEY,
            secure=False
        )

    def get_download_link(self, bucket_name, object_name, expires=timedelta(days=7)):
        if not self.connection.bucket_exists(bucket_name):
            return None
        try:
            link = self.connection.presigned_get_object(bucket_name, object_name, expires=expires)
        except minio.error.NoSuchKey:
            link = None
        return link

    def get_upload_link(self, bucket_name, object_name, expires=timedelta(days=7)):
        if not self.connection.bucket_exists(bucket_name):
            self.connection.make_bucket(bucket_name)
        return self.connection.presigned_put_object(bucket_name, object_name, expires=expires)

    def download_object(self, bucket_name, object_name, file_path):
        if not self.connection.bucket_exists(bucket_name):
            return None
        try:
            self.connection.fget_object(bucket_name, object_name, file_path)
        except minio.error.NoSuchKey:
            return None

    def put_object(self, bucket_name, object_name, file_path):
        if not self.connection.bucket_exists(bucket_name):
            self.connection.make_bucket(bucket_name)
        return self.connection.fput_object(bucket_name, object_name, file_path)

    def get_objects_list(self, bucket_name):
        objects_list = []
        objects = self.connection.list_objects(bucket_name)
        for obj in objects:
            objects_list.append(obj.object_name)
        return objects_list

    def get_buckets_list(self, ):
        buckets_list = []
        buckets = self.connection.list_buckets()
        for bucket in buckets:
            buckets_list.append(bucket.name)
        return buckets_list
