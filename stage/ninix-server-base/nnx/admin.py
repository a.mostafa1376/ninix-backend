from django.contrib import admin

from nnx.entities.authorization.kid_model import Kid
from nnx.entities.authorization.member_model import Member
from nnx.entities.device.boot_loader_model import BootLoader
from nnx.entities.device.device_parameter_model import DeviceParameter
from nnx.entities.device.firmware_model import Firmware
from nnx.entities.device.hub_firmware_model import HubFirmware
from nnx.entities.log.raw_log_model import RawLog
from nnx.entities.log.tracking_log_model import TrackingLog
from nnx.entities.media.image_file_model import ImageFile
from nnx.entities.media.image_profile_model import ImageProfile

admin.site.register(Kid)
admin.site.register(Member)
admin.site.register(BootLoader)
admin.site.register(DeviceParameter)
admin.site.register(Firmware)
admin.site.register(HubFirmware)
admin.site.register(RawLog)
admin.site.register(TrackingLog)
admin.site.register(ImageFile)
admin.site.register(ImageProfile)
