from hs_infra.enums.base.base_enum import BaseEnum


class LogLevelType(BaseEnum):
    all = 0
    debug = 10
    info = 20
    warning = 30
    error = 40
    fatal = 50
    off = 90
