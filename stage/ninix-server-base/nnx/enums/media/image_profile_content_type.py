from hs_infra.enums.base.base_enum import BaseEnum


class ImageProfileContentType(BaseEnum):
    kid = 0
    member = 1
