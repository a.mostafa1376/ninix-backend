from hs_infra.enums.base.base_enum import BaseEnum


class CommonAspectRatios(BaseEnum):
    _1_1 = 0
    _16_9 = 1
    _4_3 = 2
    _3_2 = 3
    unknown = 9
