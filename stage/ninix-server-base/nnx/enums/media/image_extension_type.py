from hs_infra.enums.base.base_enum import BaseEnum


class ImageExtensionType(BaseEnum):
    jpg = 1
    jpeg = 2
    svg = 3
    png = 4
    unknown = 9
