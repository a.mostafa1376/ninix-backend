from hs_infra.enums.base.base_enum import BaseEnum


class RelationshipType(BaseEnum):
    mother = 0
    father = 1
    aunt = 2
    uncle = 3
    sister = 4
    brother = 5
    baby_sitter = 6
    grand_mother = 10
    grand_father = 11
