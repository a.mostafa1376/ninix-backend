from hs_infra.enums.base.base_enum import BaseEnum


class GenderType(BaseEnum):
    girl = 0
    boy = 1
