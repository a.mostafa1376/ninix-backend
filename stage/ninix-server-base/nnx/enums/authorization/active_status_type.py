from hs_infra.enums.base.base_enum import BaseEnum


class ActiveStatusType(BaseEnum):
    initial = 0
    active = 1
    verification_required = 2
    disabled = 8
    banned = 9
