import os

from hs_infra.managers.base.base_manager import BaseManager
from hs_infra.meta_classes.singleton_meta_class import Singleton

from ninix.config.runtime_config import RuntimeConfig
from nnx.dal.device.hub_firmware_dal import HubFirmwareDal
from nnx.entities.device.hub_firmware_model import HubFirmware


class HubFirmwareManager(BaseManager, metaclass=Singleton):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.hub_firmware_dal = HubFirmwareDal()

    def get_latest_hub_firmware(self, version_code: int) -> HubFirmware:
        hub_firmware = None
        hub_firmware_qs = self.hub_firmware_dal.get_by_version_code(version_code)
        if hub_firmware_qs.exists():
            hub_firmware = hub_firmware_qs[0]
        return hub_firmware

    def save_hub_firmware(self, validated_data, hub_firmware_file):
        version_no = validated_data.get(HubFirmware.version_no.field_name)
        version_code = validated_data.get(HubFirmware.version_code.field_name)
        change_log = validated_data.get(HubFirmware.change_log.field_name)
        hub_firmware_src = RuntimeConfig.EXTERNAL_ADDRESS + hub_firmware_file.name
        hub_firmware = self.hub_firmware_dal.create_new_hub_firmware(version_no, version_code, change_log, hub_firmware_src)
        if isinstance(hub_firmware, HubFirmware):
            hub_firmware_exist = False
            path = os.path.join(RuntimeConfig.STATIC_ROOT, hub_firmware_file.name)
            with open(path, 'wb+') as destination:
                for chunk in hub_firmware_file.chunks():
                    destination.write(chunk)
        else:
            hub_firmware_exist = True
        return hub_firmware_exist, hub_firmware_src
