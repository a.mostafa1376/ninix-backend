import os
from typing import Tuple

from hs_infra.managers.base.base_manager import BaseManager
from hs_infra.meta_classes.singleton_meta_class import Singleton

from ninix.config.runtime_config import RuntimeConfig
from nnx.dal.device.boot_loader_dal import BootLoaderDal
from nnx.entities.device.boot_loader_model import BootLoader


class BootLoaderManager(BaseManager, metaclass=Singleton):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.boot_loader_dal = BootLoaderDal()

    def get_latest_boot_loader(self, version_code: int) -> BootLoader:
        boot_loader = None
        boot_loader_qs = self.boot_loader_dal.get_by_version_code(version_code)
        if boot_loader_qs.exists():
            boot_loader = boot_loader_qs[0]
        return boot_loader

    def save_boot_loader(self, validated_data, boot_loader_file) -> Tuple[bool, str]:
        version_no = validated_data.get(BootLoader.version_no.field_name)
        version_code = validated_data.get(BootLoader.version_code.field_name)
        change_log = validated_data.get(BootLoader.change_log.field_name)
        boot_loader_src = RuntimeConfig.EXTERNAL_ADDRESS + boot_loader_file.name
        boot_loader = self.boot_loader_dal.create_new_boot_loader(version_no, version_code, change_log, boot_loader_src)
        if isinstance(boot_loader, BootLoader):
            boot_loader_exist = False
            path = os.path.join(RuntimeConfig.STATIC_ROOT, boot_loader_file.name)
            with open(path, 'wb+') as destination:
                for chunk in boot_loader_file.chunks():
                    destination.write(chunk)
        else:
            boot_loader_exist = True
        return boot_loader_exist, boot_loader_src
