from typing import List, Dict

from hs_infra.managers.base.base_manager import BaseManager
from hs_infra.meta_classes.singleton_meta_class import Singleton
from hs_infra.utils.utils import Utils

from nnx.dal.device.device_parameter_dal import DeviceParameterDal
from nnx.entities.authorization.kid_model import Kid
from nnx.entities.device.device_parameter_model import DeviceParameter


class DeviceParameterManager(BaseManager, metaclass=Singleton):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.dal = DeviceParameterDal()

    def get_all_parameters(self) -> List[DeviceParameter]:
        parameters_list = self.dal.get_all_parameters()
        return parameters_list

    @staticmethod
    def populate_evaluated_params(kid: Kid, param_list: List[Dict]) -> List[Dict]:
        age = Utils.get_current_datetime() - kid.birth_date
        for param in param_list:
            param_value = param.get(DeviceParameter.parameter_value.field_name)
            param_formula = param.get(DeviceParameter.parameter_formula.field_name)
            __globals = {
                'a': age.days,
                'v': param_value,
            }
            param['parameter_eval'] = eval(param_formula, __globals)
        return param_list
