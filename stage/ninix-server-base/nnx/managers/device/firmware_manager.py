import os

from hs_infra.managers.base.base_manager import BaseManager
from hs_infra.meta_classes.singleton_meta_class import Singleton

from ninix.config.runtime_config import RuntimeConfig
from nnx.dal.device.firmware_dal import FirmwareDal
from nnx.entities.device.firmware_model import Firmware


class FirmwareManager(BaseManager, metaclass=Singleton):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.firmware_dal = FirmwareDal()

    def get_latest_firmware(self, version_code: int) -> Firmware:
        firmware = None
        firmware_qs = self.firmware_dal.get_by_version_code(version_code)
        if firmware_qs.exists():
            firmware = firmware_qs[0]
        return firmware

    def save_firmware(self, validated_data, firmware_file):
        version_no = validated_data.get(Firmware.version_no.field_name)
        version_code = validated_data.get(Firmware.version_code.field_name)
        change_log = validated_data.get(Firmware.change_log.field_name)
        firmware_src = RuntimeConfig.EXTERNAL_ADDRESS + firmware_file.name
        firmware = self.firmware_dal.create_new_firmware(version_no, version_code, change_log, firmware_src)
        if isinstance(firmware, Firmware):
            firmware_exist = False
            path = os.path.join(RuntimeConfig.STATIC_ROOT, firmware_file.name)
            print(path)
            with open(path, 'wb+') as destination:
                for chunk in firmware_file.chunks():
                    destination.write(chunk)
        else:
            firmware_exist = True
        return firmware_exist, firmware_src
