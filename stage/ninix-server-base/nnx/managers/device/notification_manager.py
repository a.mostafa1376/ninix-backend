from typing import Dict

from hs_infra.managers.base.base_manager import BaseManager
from hs_infra.meta_classes.singleton_meta_class import Singleton

from nnx.dal.device.notification_dal import NotificationDal
from nnx.entities.device.notification_model import Notification


class NotificationManager(BaseManager, metaclass=Singleton):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.dal = NotificationDal()

    def create_notification(self, validated_data: Dict) -> Notification:
        criteria = {
            Notification.serial_number.field_name: validated_data.get(Notification.serial_number.field_name),
            Notification.notification_type.field_name: validated_data.get(Notification.notification_type.field_name),
            Notification.timestamp.field_name: validated_data.get(Notification.timestamp.field_name),
        }
        return self.dal.create_new_notification(**criteria)

    def get_last_notification(self) -> Notification:
        notification = None
        notification_qs = self.dal.get_last_notification()
        if notification_qs.exists():
            notification = notification_qs[0]
        return notification
