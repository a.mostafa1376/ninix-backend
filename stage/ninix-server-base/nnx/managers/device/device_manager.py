from hs_infra.managers.base.base_manager import BaseManager
from hs_infra.meta_classes.singleton_meta_class import Singleton

from nnx.dal.device.device_dal import DeviceDal
from nnx.entities.device.device_model import Device


class DeviceManager(BaseManager, metaclass=Singleton):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.dal = DeviceDal()

    def create_device(self) -> Device:
        return self.dal.create_new_device()
