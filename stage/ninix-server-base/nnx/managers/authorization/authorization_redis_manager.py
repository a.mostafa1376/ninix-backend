import json
import time
from typing import Optional, Union

from hs_infra.managers.base.base_redis_manager import BaseRedisManager
from hs_infra.meta_classes.singleton_meta_class import Singleton

from ninix.config.runtime_config import RuntimeConfig


class AuthorizationRedisManager(BaseRedisManager, metaclass=Singleton):
    TIMESTAMP_KEY = 'timestamp'
    PASSCODE_KEY = 'passcode'
    FAILED_KEY = 'failed'
    BAN_HISTORY_KEY = 'ban_history'
    BANNED_KEY = 'banned'
    KEY_SEPARATOR = ':'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def set_passcode(self, id_, code, value=1):
        current_time = time.time()
        value = {
            self.PASSCODE_KEY: code,
            self.TIMESTAMP_KEY: current_time,
        }
        value = json.dumps(value)
        self.get_connection().expire(id_, RuntimeConfig.PASSCODE_TTL)
        return self.get_connection().rpush(id_, value)

    def get_passcode(self, id_, code) -> Optional[str]:
        codes_list = self.get_connection().lrange(id_, -1, -1)
        response = None
        if codes_list is None or len(codes_list) == 0:
            response = None
        else:
            value = codes_list[-1]
            value = value.decode('utf-8')
            value = json.loads(value)
            passcode = value.get(self.PASSCODE_KEY)
            if code == passcode:
                self.get_connection().delete(id_)
                response = code
        return response

    def live_requests(self, id_) -> int:
        passcode_list = self.get_connection().lrange(id_, 0, -1)
        live_requests = 0
        current_time = time.time()
        for passcode_bundle in passcode_list:
            passcode_bundle = passcode_bundle.decode('utf-8')
            passcode_timestamp = self._get_passcode_timestamp(passcode_bundle)
            if current_time - passcode_timestamp <= RuntimeConfig.PASSCODE_TTL:
                live_requests += 1
        return live_requests

    def _get_passcode_timestamp(self, passcode_bundle: Union[str, dict]) -> float:
        if isinstance(passcode_bundle, str):
            data = json.loads(passcode_bundle)
        elif isinstance(passcode_bundle, dict):
            data = passcode_bundle
        else:
            raise ValueError()
        timestamp = data.get(self.TIMESTAMP_KEY)
        return timestamp

    def ban_user(self, id_):
        key = self.KEY_SEPARATOR.join([id_, self.BANNED_KEY])
        ban_history_key = self.KEY_SEPARATOR.join([id_, self.BAN_HISTORY_KEY])
        banned_history = self.get_connection().get(ban_history_key)
        if not banned_history:
            user_ban_ttl = RuntimeConfig.USER_BAN_TTL
        else:
            user_ban_ttl = RuntimeConfig.USER_BAN_TTL_LONG
        self.get_connection().set(ban_history_key, value=1, ex=RuntimeConfig.BAN_HISTORY_TTL)
        return self.get_connection().set(key, value=1, ex=user_ban_ttl)

    def is_user_banned(self, id_):
        key = self.KEY_SEPARATOR.join([id_, self.BANNED_KEY])
        response = False
        if self.get_connection().get(key):
            response = True
        return response

    def add_failed_attempt_to_user(self, id_):
        key = self.KEY_SEPARATOR.join([id_, self.FAILED_KEY])
        failed_attempts = self.get_connection().get(key)
        if not failed_attempts:
            failed_attempts = 0
        else:
            failed_attempts = int(failed_attempts)
        failed_attempts += 1
        return self.get_connection().set(key, value=failed_attempts, ex=RuntimeConfig.FAILED_ATTEMPT_TTL)

    def delete_failed_attempts(self, id_):
        key = self.KEY_SEPARATOR.join([id_, self.FAILED_KEY])
        return self.get_connection().delete(key)

    def failed_attempts(self, id_):
        key = self.KEY_SEPARATOR.join([id_, self.FAILED_KEY])
        failed_attempts = self.get_connection().get(key)
        if not failed_attempts:
            failed_attempts = 0
        else:
            failed_attempts = int(failed_attempts)
        return failed_attempts

    def delete_ban_history(self, id_):
        key = self.KEY_SEPARATOR.join([id_, self.BAN_HISTORY_KEY])
        return self.get_connection().delete(key)
