from typing import Dict

from hs_infra.managers.base.base_manager import BaseManager
from hs_infra.meta_classes.singleton_meta_class import Singleton

from nnx.dal.authorization.member_dal import MemberDal
from nnx.entities.authorization.member_model import Member
from nnx.enums.authorization.relationship_type import RelationshipType


class MemberManager(BaseManager, metaclass=Singleton):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.member_dal = MemberDal()

    def create_member(self, validated_data: Dict) -> Member:
        first_name = validated_data.get(Member.first_name.field_name)
        last_name = validated_data.get(Member.last_name.field_name)
        mobile_number = validated_data.get(Member.mobile_number.field_name)
        relationship = validated_data.get(Member.relationship_type.field_name)
        birth_year = validated_data.get(Member.birth_year.field_name)
        
        print(birth_year)
        
        relationship_type = RelationshipType.get_type_by_value(relationship)
        member = self.member_dal.create_new_member(
            first_name=first_name,
            last_name=last_name,
            mobile_number=mobile_number,
            relationship_type=relationship_type,
            birth_year=birth_year,
        )
        return member

    def get_image_profiles_by_member_id(self, object_id):
        member = self.find_member_by_id(object_id)
        if member is None:
            image_profiles_list = []
        else:
            image_profiles_list = member.image_profile.all()
        return image_profiles_list

    def find_member_by_id(self, object_id):
        return self.member_dal.find_by_id(object_id)
