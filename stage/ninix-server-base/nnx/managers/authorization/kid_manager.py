from typing import Dict

from hs_infra.managers.base.base_manager import BaseManager
from hs_infra.meta_classes.singleton_meta_class import Singleton

from nnx.dal.authorization.kid_dal import KidDal
from nnx.entities.authorization.kid_model import Kid


class KidManager(BaseManager, metaclass=Singleton):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.kid_dal = KidDal()

    def create_kid(self, validated_data: Dict, member) -> Kid:
        first_name = validated_data.get(Kid.first_name.field_name)
        last_name = validated_data.get(Kid.last_name.field_name)
        gender = validated_data.get(Kid.gender.field_name)
        birth_date = validated_data.get(Kid.birth_date.field_name)
        device_uid = validated_data.get(Kid.device_uid.field_name)

        kid = self.kid_dal.create_new_kid(first_name, last_name, gender, birth_date, device_uid)
        kid.members.add(member)
        return kid

    def get_kid_by_device_uid(self, device_uid, member) -> Kid:
        kid = self.kid_dal.get_by_device_uid(device_uid)
        if kid and kid not in member.kids.all():
            kid.members.add(member)
        return kid

    def find_kid_by_id(self, object_id):
        return self.kid_dal.find_by_id(object_id)

    def get_image_profiles_by_kid_id(self, object_id):
        kid = self.find_kid_by_id(object_id)
        if kid is None:
            image_profiles = []
        else:
            image_profiles = kid.image_profile.all()
        return image_profiles
