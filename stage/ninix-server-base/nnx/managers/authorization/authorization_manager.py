from typing import Any, Tuple, List, Union

from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from hs_infra.managers.base.base_manager import BaseManager
from hs_infra.meta_classes.singleton_meta_class import Singleton
from hs_infra.utils.utils import Utils
from rest_framework.exceptions import AuthenticationFailed
from rest_framework_jwt.settings import api_settings

from ninix.config.runtime_config import RuntimeConfig
from ninix.constants.messages.en.en_messages import EnMessages
from nnx.adapters.message_adapters.sms_message_adapter import SmsMessageAdapter
from nnx.adapters.message_adapters.twilio_message_adapter import TwilioMessageAdapter
from nnx.dal.authorization.member_dal import MemberDal
from nnx.entities.authorization.member_model import Member
from nnx.enums.authorization.active_status_type import ActiveStatusType
from nnx.enums.authorization.relationship_type import RelationshipType
from nnx.managers.authorization.authorization_redis_manager import AuthorizationRedisManager


class AuthorizationManager(BaseManager, metaclass=Singleton):
    MANY_ATTEMPTS_ERROR = 'too many attempts'
    PASSCODE_VALIDATION_ERROR = 'passcode is not valid'
    PERMISSION_ERROR = "you're not  allowed to perform this action"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.member_dal = MemberDal()
        self.sms_adapter = SmsMessageAdapter()
        self.twilio_sms_adapter = TwilioMessageAdapter()
        self.redis_manager = AuthorizationRedisManager()

    def _generate_jwt_token(self, user):
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
        payload = jwt_payload_handler(user)
        payload['creation_time'] = Utils.convert_to_string(Utils.get_current_datetime())
        token = jwt_encode_handler(payload)
        return token

    def authenticate_user(self, mobile_number, request, password=None):
        if password is None:
            password = RuntimeConfig.DEFAULT_PASSWORD
        user = authenticate(username=mobile_number, password=password)
        if user is None:
            raise AuthenticationFailed()
        login(request, user)
        token = self._generate_jwt_token(user)
        return token

    def get_member(self, mobile_number: str, active_status_type_list: List[int] = None) -> Union[Member, None]:
        member = None
        member_qs = self.member_dal.find_member_by_mobile_number(mobile_number)
        if member_qs.exists():
            member = member_qs[0]
            if active_status_type_list is not None:
                if member.active_status_type not in active_status_type_list:
                    member = None
        return member

    def _generate_passcode(self, identifier: str):
        passcode = Utils.generate_uuid_token(length=5, numeric=True)
        error = ''
        if not RuntimeConfig.IGNORE_PROTECT_MODE:
            live_requests = self.redis_manager.live_requests(identifier)
            if live_requests >= RuntimeConfig.ALLOWED_PASSCODE_ATTEMPTS:
                self.redis_manager.ban_user(identifier)
            if self.redis_manager.is_user_banned(identifier):
                passcode = None
                error = self.MANY_ATTEMPTS_ERROR
                return passcode, error
        self.redis_manager.set_passcode(identifier, passcode)
        return passcode, error

    def generate_passcode(self, mobile_number: str) -> Tuple[bool, str]:
        member_qs = self.member_dal.find_member_by_mobile_number(mobile_number)
        if member_qs.exists():
            member = member_qs[0]
            if member.active_status_type in [ActiveStatusType.banned.value, ActiveStatusType.disabled.value]:
                response = False
                error = self.PERMISSION_ERROR
                return response, error
        else:
            _ = self.get_or_create_user(mobile_number)
        passcode, error = self._generate_passcode(mobile_number)
        if passcode is not None:
            self.send_passcode(passcode, mobile_number)
            if RuntimeConfig.DEBUG_MODE:
                print('passcode:', passcode, '\tmobile_number:', mobile_number)
            response = True

        else:
            response = False
        return response, error

    def send_passcode(self, passcode, mobile_number):
        message = EnMessages.AUTH_PASSCODE_MESSAGE + str(passcode)
        if mobile_number.startswith("98"):
            ok = self.sms_adapter.push_to_queue(message, mobile_number, is_verification=True)
        else:
            ok = self.twilio_sms_adapter.push_to_queue(message, mobile_number)
        return ok

    def get_or_create_user(self, mobile_number) -> User:
        user = self.member_dal.get_or_create_user(mobile_number)
        return user

    def create_member(self, mobile_number, first_name, last_name, relationship_type, birth_year) -> Member:
        if isinstance(relationship_type, RelationshipType):
            relationship_type = relationship_type.value
        member = self.member_dal.create_new_member(mobile_number, first_name, last_name, relationship_type, birth_year)
        return member

    
    @staticmethod
    def is_number(s):
        try:
            int(s)
            return True
        except ValueError:
            return False
        
    def check_passcode(self, identifier, passcode) -> Tuple[bool, Any]:
        response = True
        error = ''
        if not RuntimeConfig.IGNORE_PROTECT_MODE:
            failed_attempts = self.redis_manager.failed_attempts(identifier)
            print(failed_attempts)
            banned = self.redis_manager.is_user_banned(identifier)
            if failed_attempts >= RuntimeConfig.FAILED_ATTEMPT_LIMIT - 1:
                self.redis_manager.ban_user(identifier)
            if banned:
                response = False
                error = self.MANY_ATTEMPTS_ERROR
                return response, error
        
        
        print(self.is_number(identifier))
        if self.is_number(identifier):
            value = self.redis_manager.get_passcode(identifier, passcode)
        else:
            value = False
        
            from google.oauth2 import id_token
            from google.auth.transport import requests
            try:
                idinfo = id_token.verify_oauth2_token(passcode, requests.Request())

                if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                    raise ValueError('Wrong issuer.')
                userid = idinfo['sub']

                if idinfo['email_verified'] and (idinfo['email'] == identifier):
                    value = True

            except ValueError:
                # Invalid token
                pass
        
        if not value:
            self.redis_manager.add_failed_attempt_to_user(identifier)
            response = False
            error = self.PASSCODE_VALIDATION_ERROR
        else:
            self.redis_manager.delete_failed_attempts(identifier)
            self.redis_manager.delete_ban_history(identifier)
        return response, error

    def is_registered(self, mobile_number) -> bool:
        active_status_type_list = [
            ActiveStatusType.active.value,
            ActiveStatusType.verification_required.value,
            ActiveStatusType.disabled.value,
            ActiveStatusType.banned.value,
        ]
        member = self.get_member(mobile_number, active_status_type_list)
        is_registered = False
        if member is not None:
            is_registered = True
        return is_registered
