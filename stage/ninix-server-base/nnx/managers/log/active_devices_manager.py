from typing import Dict

from hs_infra.managers.base.base_manager import BaseManager
from hs_infra.meta_classes.singleton_meta_class import Singleton

from nnx.dal.log.active_devices_dal import ActiveDevicesDal
from nnx.dal.authorization.kid_dal import KidDal

class ActiveDevicesManager(BaseManager, metaclass=Singleton):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.dal = ActiveDevicesDal()
        self.kid_dal = KidDal()

    def find_active_devices(self, from_timestamp, until_timestamp, collection=None):       
        active_device_ids = self.dal.find_active_devices(from_timestamp, until_timestamp, collection)
        active_kids = []
        for kid_id in active_device_ids:
            active_kids.append(self.kid_dal.get_by_kid_id(kid_id))
        return active_kids
