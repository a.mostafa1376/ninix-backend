from typing import Dict

from hs_infra.managers.base.base_manager import BaseManager
from hs_infra.meta_classes.singleton_meta_class import Singleton

from nnx.dal.authorization.kid_dal import KidDal
from nnx.dal.log.raw_log_dal import RawLogDal
from nnx.entities.log.raw_log_model import RawLog


class RawLogManager(BaseManager, metaclass=Singleton):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.dal = RawLogDal()
        self.kid_dal = KidDal()

    def create_log_record(self, member, validated_data: Dict) -> RawLog:
        device_uid = validated_data.get(RawLog.device_uid.field_name)
        kid = self.kid_dal.get_by_device_uid(device_uid)
        
        criteria = {
            RawLog.log_message.field_name: validated_data.get(RawLog.log_message.field_name),
            RawLog.log_level.field_name: validated_data.get(RawLog.log_level.field_name),
            RawLog.device_uid.field_name: validated_data.get(RawLog.device_uid.field_name),
            RawLog.log_data.field_name: validated_data.get(RawLog.log_data.field_name),
            'kid': kid,
            'member': member,
        }
        raw_log = self.dal.create_new_log_record(kid.id, **criteria)
        return raw_log
