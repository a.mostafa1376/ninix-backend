from typing import Dict

from hs_infra.managers.base.base_manager import BaseManager
from hs_infra.meta_classes.singleton_meta_class import Singleton

from nnx.dal.log.member_logs_dal import MemberLogsDal


class MemberLogsManager(BaseManager, metaclass=Singleton):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.dal = MemberLogsDal()

    def find_logs(self, kid_id):
        logs = self.dal.find_logs(kid_id)
        return logs
