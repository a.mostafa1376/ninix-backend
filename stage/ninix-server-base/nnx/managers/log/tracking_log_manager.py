from typing import Dict

from hs_infra.managers.base.base_manager import BaseManager
from hs_infra.meta_classes.singleton_meta_class import Singleton

from nnx.dal.authorization.kid_dal import KidDal
from nnx.dal.log.tracking_log_dal import TrackingLogDal
from nnx.entities.log.raw_log_model import RawLog
from nnx.entities.log.tracking_log_model import TrackingLog


class TrackingLogManager(BaseManager, metaclass=Singleton):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.dal = TrackingLogDal()
        self.kid_dal = KidDal()

    def find_log_record(self, device_uid, data, collection=None):
        kid = self.kid_dal.get_by_device_uid(device_uid)
        if kid is None:
            return None
        track_log = self.dal.find_log(kid.id, data, collection)
        return track_log

    def create_log_record(self, member, device_uid, validated_data: Dict, overwrite, collection=None) -> RawLog:
        kid = self.kid_dal.get_by_device_uid(device_uid)
        criteria = {
            TrackingLog.log_data.field_name: validated_data.get(TrackingLog.log_data.field_name),
            TrackingLog.timestamp.field_name: validated_data.get(TrackingLog.timestamp.field_name),
            'kid': kid,
            'member': member,
        }
        tracking_log = self.dal.create_new_log_record(kid.id, overwrite, collection, **criteria)
        return tracking_log
