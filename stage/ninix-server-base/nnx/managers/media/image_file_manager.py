from PIL import Image

from nnx.adapters.image_adapters.image_magick_adapter import ImageMagickAdapter
from nnx.adapters.minio_adapter.minio_adapter import MinioAdapter
from nnx.dal.media.image_file_dal import ImageFileDal
from nnx.dal.media.image_profile_dal import ImageProfileDal
from nnx.entities.media.image_file_model import ImageFile
from nnx.enums.media.common_aspect_ratios import CommonAspectRatios
from nnx.managers.media.base_image_manager import BaseImageManager


class ImageFileManager(BaseImageManager):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.image_file_dal = ImageFileDal()
        self.image_profile_dal = ImageProfileDal()
        self.image_adapter = ImageMagickAdapter()
        self.minio_adapter = MinioAdapter()

    def save_image_file(self, image_profile_name, aspect_ratio: float = 1) -> ImageFile:
        ext = self.FILES_EXTENSION
        aspect_ratio_identify = self._aspect_ratio_identify(aspect_ratio)
        profile_pure_name = image_profile_name.split('.')[0]
        file_name = self.FILE_NAME.format(
            image_profile_name=profile_pure_name,
            aspect_ratio_str=aspect_ratio_identify,
            extension=ext
        )
        file_absolute_destination = self.IMAGE_FILE_ABSOLUTE_PATH.format(file_name)

        image_profile_absolute_path = self.IMAGE_PROFILE_ABSOLUTE_PATH.format(image_profile_name)
        image_profile_local_path = self.IMAGE_PROFILE_LOCAL_PATH.format(image_profile_name)
        image_profile = self.image_profile_dal.get_by_address(image_profile_local_path)

        self.create_image_file_ratio(file_absolute_destination, image_profile_absolute_path, aspect_ratio)
        self.minio_adapter.put_object(self.IMAGE_FILES_BUCKET, file_name, file_absolute_destination)

        file_absolute_path = self.IMAGE_FILE_ABSOLUTE_PATH.format(file_name)
        file_local_path = self.IMAGE_FILE_LOCAL_PATH.format(file_name)

        image_file = Image.open(file_absolute_path)
        width, height = image_file.size
        image_file.close()

        aspect_ratio_number = self._aspect_ratio_number(aspect_ratio)

        image_file = self.image_file_dal.create_image_file(
            file_path=file_local_path,
            file_name=file_name,
            image_profile=image_profile,
            ext=ext,
            width=width,
            height=height,
            aspect_ratio=aspect_ratio_number
        )

        if aspect_ratio == self.image_adapter.RATIO_1_1:
            small_file_width = self.SMALL_FILE_WIDTH
            small_file_height = self.SMALL_FILE_HEIGHT
            small_file_name = self.FILE_NAME.format(
                image_profile_name=profile_pure_name,
                aspect_ratio_str=aspect_ratio_identify + '_small',
                extension=ext
            )
            small_file_absolute_path = self.IMAGE_FILE_ABSOLUTE_PATH.format(small_file_name)
            small_file_local_path = self.IMAGE_FILE_LOCAL_PATH.format(small_file_name)

            self.create_image_file_size(
                small_file_absolute_path,
                file_absolute_path,
                small_file_width,
                small_file_height
            )
            self.minio_adapter.put_object(self.IMAGE_FILES_BUCKET, small_file_name, small_file_absolute_path)

            self.image_file_dal.create_image_file(
                file_path=small_file_local_path,
                file_name=small_file_name,
                image_profile=image_profile,
                ext=ext,
                width=small_file_width,
                height=small_file_height,
                aspect_ratio=aspect_ratio_number
            )

        return image_file

    def create_image_file_ratio(self, file_absolute_destination, image_profile_absolute_path, aspect_ratio: float = 1):
        self.image_adapter.minimal_crop_image(image_profile_absolute_path, file_absolute_destination, aspect_ratio)

    def create_image_file_size(self, file_absolute_destination, image_profile_absolute_path, width, height):
        self.image_adapter.absolute_resize_image(image_profile_absolute_path, file_absolute_destination, width, height)

    def get_download_link(self, image_file_name):
        return self.minio_adapter.get_download_link(
            bucket_name=self.IMAGE_FILES_BUCKET,
            object_name=image_file_name,
            expires=self.DOWNLOAD_LINK_EXPIRY_TIME
        )

    def _aspect_ratio_number(self, aspect_ratio: float = 1) -> str:
        if aspect_ratio == self.image_adapter.RATIO_16_9:
            aspect_ratio_number = CommonAspectRatios._16_9.value
        elif aspect_ratio == self.image_adapter.RATIO_4_3:
            aspect_ratio_number = CommonAspectRatios._4_3.value
        elif aspect_ratio == self.image_adapter.RATIO_3_2:
            aspect_ratio_number = CommonAspectRatios._3_2.value
        else:
            aspect_ratio_number = CommonAspectRatios._1_1.value

        return aspect_ratio_number

    def _aspect_ratio_identify(self, aspect_ratio: float = 1) -> str:
        if aspect_ratio == self.image_adapter.RATIO_16_9:
            aspect_ratio_id = '_16_9'
        elif aspect_ratio == self.image_adapter.RATIO_4_3:
            aspect_ratio_id = '_4_3'
        elif aspect_ratio == self.image_adapter.RATIO_3_2:
            aspect_ratio_id = '_3_2'
        else:
            aspect_ratio_id = '_1_1'

        return aspect_ratio_id