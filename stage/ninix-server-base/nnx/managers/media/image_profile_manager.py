from hs_infra.utils.utils import Utils
from rest_framework import status

from nnx.adapters.image_adapters.image_magick_adapter import ImageMagickAdapter
from nnx.adapters.minio_adapter.minio_adapter import MinioAdapter
from nnx.dal.media.image_profile_dal import ImageProfileDal
from nnx.entities.media.image_profile_model import ImageProfile
from nnx.enums.media.image_extension_type import ImageExtensionType
from nnx.enums.media.image_profile_content_type import ImageProfileContentType
from nnx.managers.authorization.kid_manager import KidManager
from nnx.managers.authorization.member_manager import MemberManager
from nnx.managers.media.base_image_manager import BaseImageManager
from nnx.managers.media.image_file_manager import ImageFileManager


class ImageProfileManager(BaseImageManager):
    PERMISSION_ERROR = 'you do not have permission to perform this action'
    SERVER_ERROR = 'internal server error'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.image_adapter = ImageMagickAdapter()
        self.image_profile_dal = ImageProfileDal()
        self.image_file_manager = ImageFileManager()
        self.minio_adapter = MinioAdapter()

    def save_image_profile(self, file, object_id, member, ext=None, content_type=ImageProfileContentType.kid.value):
        new_image_file = None
        response_status = status.HTTP_200_OK
        error_msg = ''
        if content_type == ImageProfileContentType.kid.value:
            kid_manager = KidManager()
            obj = kid_manager.find_kid_by_id(object_id)
            if not obj or obj not in member.kids.all():
                response_status = status.HTTP_403_FORBIDDEN
                error_msg = self.PERMISSION_ERROR
                return new_image_file, response_status, error_msg
        else:
            member_manager = MemberManager()
            obj = member_manager.find_member_by_id(object_id)
            if not obj or obj != member:
                response_status = status.HTTP_403_FORBIDDEN
                error_msg = self.PERMISSION_ERROR
                return new_image_file, response_status, error_msg
        new_image_profile = self.image_profile_dal.create_image_profile(
            file=file,
            file_name=file.name,
            ext=ext,
            content_object=obj
        )
        if not new_image_profile:
            response_status = status.HTTP_500_INTERNAL_SERVER_ERROR
            error_msg = self.SERVER_ERROR
        self.minio_adapter.put_object(
            bucket_name=self.IMAGE_PROFILE_BUCKET,
            object_name=file.name,
            file_path=self.IMAGE_PROFILE_ABSOLUTE_PATH.format(file.name)
        )
        return new_image_profile, response_status, error_msg

    def create_files(self, image_profile_name):
        aspect_ratio_list = [
            self.image_adapter.RATIO_1_1,
            self.image_adapter.RATIO_3_2,
            self.image_adapter.RATIO_4_3,
            self.image_adapter.RATIO_16_9,
        ]
        for aspect_ratio in aspect_ratio_list:
            self.image_file_manager.save_image_file(image_profile_name, aspect_ratio=aspect_ratio)

    def get_image_profile_name_by_id(self, object_id):
        image_profile = self.image_profile_dal.get_image_profile_by_id(object_id)
        if image_profile is None:
            image_profile_name = None
        else:
            image_profile_name = image_profile.file_name
        return image_profile_name

    def get_download_link(self, image_profile_name):
        self.minio_adapter.get_download_link(self.IMAGE_PROFILE_BUCKET, image_profile_name)

    def get_image_files_link(self, image_profile: ImageProfile):
        image_files_link = []
        for image_file in image_profile.image_files.all():
            image_file_link = self.image_file_manager.get_download_link(image_file.file_name)
            image_files_link.append(image_file_link)
        return image_files_link

    @classmethod
    def generate_image_name(cls, ext: int = None) -> str:
        name = Utils.generate_uuid_token()
        if ext:
            name = '.'.join([name, ImageExtensionType.get_name_by_value(ext)])
        return name
