import os
from abc import ABC
from datetime import timedelta

from hs_infra.managers.base.base_manager import BaseManager
from hs_infra.meta_classes.singleton_meta_class import Singleton

from ninix.config.runtime_config import RuntimeConfig


class BaseImageManager(BaseManager, ABC):
    # Image files constants
    IMAGE_FILES_BUCKET = 'imagefiles'
    IMAGE_FILES_DIRECTORY = 'image_files'
    IMAGE_FILES_ROOT = os.path.join(RuntimeConfig.MEDIA_ROOT, IMAGE_FILES_DIRECTORY)

    IMAGE_FILE_ABSOLUTE_PATH = os.path.join(IMAGE_FILES_ROOT, '{}')
    IMAGE_FILE_LOCAL_PATH = '/'.join([IMAGE_FILES_DIRECTORY, '{}'])

    FILE_NAME = '{image_profile_name}{aspect_ratio_str}.{extension}'
    FILES_EXTENSION = 'jpg'
    SMALL_FILE_WIDTH = 200
    SMALL_FILE_HEIGHT = 200

    # Image profile constants
    IMAGE_PROFILE_BUCKET = 'imageprofile'
    IMAGE_PROFILE_DIRECTORY = 'image_profile'
    IMAGE_PROFILE_ROOT = os.path.join(RuntimeConfig.MEDIA_ROOT, IMAGE_PROFILE_DIRECTORY)

    IMAGE_PROFILE_ABSOLUTE_PATH = os.path.join(IMAGE_PROFILE_ROOT, '{}')
    IMAGE_PROFILE_LOCAL_PATH = '/'.join([IMAGE_PROFILE_DIRECTORY, '{}'])

    # Common constants
    DOWNLOAD_LINK_EXPIRY_TIME = timedelta(minutes=2)
