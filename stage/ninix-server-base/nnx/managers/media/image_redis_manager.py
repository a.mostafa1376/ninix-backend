from hs_infra.managers.base.base_redis_manager import BaseRedisManager
from hs_infra.meta_classes.singleton_meta_class import Singleton


class ImageRedisManager(BaseRedisManager, metaclass=Singleton):
    IMAGES_KEY = 'images'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def push_to_queue(self, image_profile_name):
        return self.get_connection().rpush(self.IMAGES_KEY, image_profile_name)

    def read_image_name_from_queue(self):
        value = self.get_connection().lpop(self.IMAGES_KEY)
        if not value:
            response = None
        else:
            file_name = value.decode('utf-8')
            response = file_name
        return response
