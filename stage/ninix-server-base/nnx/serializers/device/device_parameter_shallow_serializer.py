from rest_framework import serializers

from nnx.entities.device.device_parameter_model import DeviceParameter


class DeviceParameterShallowSerializer(serializers.ModelSerializer):
    class Meta:
        model = DeviceParameter
        fields = (
            model.id.field_name,
            model.parameter_name.field_name,
            model.parameter_detail.field_name,
            model.parameter_value.field_name,
            model.parameter_formula.field_name,
        )
