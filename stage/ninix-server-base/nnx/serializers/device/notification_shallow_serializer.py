from rest_framework import serializers

from nnx.entities.device.notification_model import Notification


class NotificationShallowSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notification
        fields = (
            model.serial_number.field_name,
            model.notification_type.field_name,
            model.timestamp.field_name,
        )
