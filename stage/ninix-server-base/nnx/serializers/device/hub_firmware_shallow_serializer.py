from rest_framework import serializers

from nnx.entities.device.hub_firmware_model import HubFirmware


class HubFirmwareShallowSerializer(serializers.ModelSerializer):
    class Meta:
        model = HubFirmware
        fields = (
            model.version_code.field_name,
            model.version_no.field_name,
            model.change_log.field_name,
            model.hub_firmware_src.field_name,
        )
