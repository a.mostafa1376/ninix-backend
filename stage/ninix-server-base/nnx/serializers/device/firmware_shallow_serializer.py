from rest_framework import serializers

from nnx.entities.device.firmware_model import Firmware


class FirmwareShallowSerializer(serializers.ModelSerializer):
    class Meta:
        model = Firmware
        fields = (
            model.version_code.field_name,
            model.version_no.field_name,
            model.change_log.field_name,
            model.firmware_src.field_name,
        )
