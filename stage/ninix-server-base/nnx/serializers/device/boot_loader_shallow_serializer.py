from rest_framework import serializers

from nnx.entities.device.boot_loader_model import BootLoader


class BootLoaderShallowSerializer(serializers.ModelSerializer):
    class Meta:
        model = BootLoader
        fields = (
            model.version_code.field_name,
            model.version_no.field_name,
            model.change_log.field_name,
            model.boot_loader_src.field_name,
        )
