from rest_framework import serializers

from nnx.entities.media.image_file_model import ImageFile


class ImageFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = ImageFile
        fields = (
            'id',
            model.width.field_name,
            model.height.field_name
        )
