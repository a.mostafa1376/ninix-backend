from rest_framework import serializers

from nnx.entities.media.image_profile_model import ImageProfile


class ImageProfileDeepSerializer(serializers.ModelSerializer):
    class Meta:
        model = ImageProfile
        fields = (
            ImageProfile.id.field_name,
            ImageProfile.object_id.field_name,
            'file',
        )


class ImageProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = ImageProfile
        fields = (
            ImageProfile.id.field_name,
            ImageProfile.object_id.field_name,
        )
