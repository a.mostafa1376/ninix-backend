from rest_framework import serializers

from nnx.entities.log.raw_log_model import RawLog


class RawLogShallowSerializer(serializers.ModelSerializer):
    class Meta:
        model = RawLog
        fields = (
            model.log_message.field_name,
            model.log_level.field_name,
            model.device_uid.field_name,
            model.log_data.field_name,
        )
