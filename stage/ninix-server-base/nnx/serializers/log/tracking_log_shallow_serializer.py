from rest_framework import serializers

from nnx.entities.log.tracking_log_model import TrackingLog


class TrackingLogShallowSerializer(serializers.ModelSerializer):
    class Meta:
        model = TrackingLog
        fields = (
            model.log_data.field_name,
            model.timestamp.field_name,
        )
