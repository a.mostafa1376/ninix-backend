from rest_framework import serializers

from nnx.entities.authorization.member_model import Member
from nnx.serializers.authorizations.kid_shallow_serializer import KidShallowSerializer


class MemberDeepSerializer(serializers.ModelSerializer):
    class Meta:
        model = Member
        fields = (
            model.id.field_name,
            model.first_name.field_name,
            model.last_name.field_name,
            model.mobile_number.field_name,
            model.birth_year.field_name,
            model.active_status_type.field_name,
            model.relationship_type.field_name,
            'kid',
        )

    kid = KidShallowSerializer(read_only=True)
