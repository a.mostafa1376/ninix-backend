from rest_framework import serializers

from nnx.entities.authorization.kid_model import Kid
from nnx.serializers.authorizations.member_shallow_serializer import MemberShallowSerializer
from nnx.serializers.media.image_profile_serializer import ImageProfileSerializer


class KidDeepSerializer(serializers.ModelSerializer):
    images_set = ImageProfileSerializer()

    class Meta:
        model = Kid
        fields = (
            model.id.field_name,
            model.first_name.field_name,
            model.last_name.field_name,
            model.gender.field_name,
            model.birth_date.field_name,
            model.images_set.field_name,
            'member',
        )

    member = MemberShallowSerializer(read_only=True)
