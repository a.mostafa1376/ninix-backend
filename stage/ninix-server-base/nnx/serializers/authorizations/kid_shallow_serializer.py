from rest_framework import serializers

from nnx.entities.authorization.kid_model import Kid


class KidShallowSerializer(serializers.ModelSerializer):
    class Meta:
        model = Kid
        fields = (
            model.id.field_name,
            model.first_name.field_name,
            model.last_name.field_name,
            model.gender.field_name,
            model.birth_date.field_name,
            model.device_uid.field_name,
        )
