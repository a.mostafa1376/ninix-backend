import json

from channels.generic.websocket import WebsocketConsumer

from ninix.config.runtime_config import RuntimeConfig


class NinixDataConsumer(WebsocketConsumer):
    def connect(self):
        self.accept()

    def disconnect(self, code):
        pass

    def receive(self, text_data=None, bytes_data=None):
        json_data = json.loads(text_data)
        if RuntimeConfig.DEBUG_MODE:
            print(json_data)
