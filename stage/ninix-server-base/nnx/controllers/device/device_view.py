from rest_framework import permissions
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from nnx.managers.device.device_manager import DeviceManager


class DeviceView(GenericAPIView):
    permission_classes = [permissions.AllowAny]
    http_method_names = ['post']

    def post(self, request, *args, **kwargs):
        manager = DeviceManager()
        device = manager.create_device()
        response_dict = {
            'serial': device.serial_code
        }
        return Response(response_dict)

    def delete(self, request, *args, **kwargs):
        pass
