from django import forms
from django.shortcuts import render
from rest_framework import permissions
from rest_framework.exceptions import NotAcceptable
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from nnx.entities.device.hub_firmware_model import HubFirmware
from nnx.managers.device.hub_firmware_manager import HubFirmwareManager
from nnx.serializers.device.hub_firmware_shallow_serializer import HubFirmwareShallowSerializer


class UploadFileForm(forms.Form):
    version_no = forms.CharField(max_length=50)
    version_code = forms.CharField(max_length=50)
    change_log = forms.CharField(max_length=500, required=False)
    hub_firmware_file = forms.FileField()


def hub_firmware_upload_view(request):
    template = 'upload_hub_firmware_template.html'
    context = None
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            hub_firmware_file = request.FILES.get('hub_firmware_file')
            manager = HubFirmwareManager()
            hub_firmware_exist, hub_firmware_src = manager.save_hub_firmware(form.data, hub_firmware_file)
            if hub_firmware_exist:
                context = {'newer_hub_firmware_exist': True}
            else:
                context = {'uploaded_file_url': hub_firmware_src}
    return render(request, template, context)


class HubFirmwareView(GenericAPIView):
    permission_classes = [permissions.AllowAny]
    http_method_names = ['get']

    @staticmethod
    def _version_code_error():
        error = {
            'version_code': [
                "version code is mandatory."
            ]
        }
        return error

    def get(self, request, *args, **kwargs):
        version_code = request.GET.get(HubFirmware.version_code.field_name)
        if not version_code:
            error = self._version_code_error()
            raise NotAcceptable(error)
        manager = HubFirmwareManager()
        version_code = int(version_code)
        hub_firmware = manager.get_latest_hub_firmware(version_code)
        if hub_firmware:
            hub_firmware_data = HubFirmwareShallowSerializer(hub_firmware).data
            available_update = True
        else:
            hub_firmware_data = None
            available_update = False
        response = {
            'availableUpdate': available_update,
            'hubFirmware': hub_firmware_data,
        }
        return Response(response)

    def post(self, request, *args, **kwargs):
        pass

    def delete(self, request, *args, **kwargs):
        pass
