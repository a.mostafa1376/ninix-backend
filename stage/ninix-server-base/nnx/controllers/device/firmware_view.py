from django import forms
from django.shortcuts import render
from rest_framework import permissions
from rest_framework.exceptions import NotAcceptable
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from nnx.entities.device.firmware_model import Firmware
from nnx.managers.device.firmware_manager import FirmwareManager
from nnx.serializers.device.firmware_shallow_serializer import FirmwareShallowSerializer


class UploadFileForm(forms.Form):
    version_no = forms.CharField(max_length=50)
    version_code = forms.CharField(max_length=50)
    change_log = forms.CharField(max_length=500, required=False)
    firmware_file = forms.FileField()


def firmware_upload_view(request):
    template = 'upload_firmware_template.html'
    context = None
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            firmware_file = request.FILES.get('firmware_file')
            manager = FirmwareManager()
            firmware_exist, firmware_src = manager.save_firmware(form.data, firmware_file)
            if firmware_exist:
                context = {'newer_firmware_exist': True}
            else:
                context = {'uploaded_file_url': firmware_src}
    return render(request, template, context)


class FirmwareView(GenericAPIView):
    permission_classes = [permissions.AllowAny]
    http_method_names = ['get']

    @staticmethod
    def _version_code_error():
        error = {
            'version_code': [
                "version code is mandatory."
            ]
        }
        return error

    def get(self, request, *args, **kwargs):
        version_code = request.GET.get(Firmware.version_code.field_name)
        if not version_code:
            error = self._version_code_error()
            raise NotAcceptable(error)
        manager = FirmwareManager()
        version_code = int(version_code)
        firmware = manager.get_latest_firmware(version_code)
        if firmware:
            firmware_data = FirmwareShallowSerializer(firmware).data
            available_update = True
        else:
            firmware_data = None
            available_update = False
        response = {
            'availableUpdate': available_update,
            'firmware': firmware_data,
        }
        return Response(response)

    def post(self, request, *args, **kwargs):
        pass

    def delete(self, request, *args, **kwargs):
        pass
