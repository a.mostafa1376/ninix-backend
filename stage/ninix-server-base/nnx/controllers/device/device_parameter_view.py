from rest_framework import permissions
from rest_framework.exceptions import NotFound
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from nnx.controllers.authorization.kid_view import KidView
from nnx.controllers.authorization.member_view import MemberView
from nnx.entities.authorization.kid_model import Kid
from nnx.managers.authorization.kid_manager import KidManager
from nnx.managers.device.device_parameter_manager import DeviceParameterManager
from nnx.serializers.device.device_parameter_shallow_serializer import DeviceParameterShallowSerializer


class DeviceParameterView(GenericAPIView):
    permission_classes = [permissions.IsAuthenticated]
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):
        device_uid = request.GET.get(Kid.device_uid.field_name)
        user = request.user
        if not hasattr(user, 'member'):
            error = MemberView.err_no_member_found()
            raise NotFound(error)
        member = user.member
        kid_manager = KidManager()
        kid = kid_manager.get_kid_by_device_uid(device_uid, member)
        if not isinstance(kid, Kid):
            error = KidView.err_no_kid_found()
            raise NotFound(error)
        manager = DeviceParameterManager()
        parameter_list = manager.get_all_parameters()
        response = DeviceParameterShallowSerializer(parameter_list, many=True).data
        response = manager.populate_evaluated_params(kid, response)
        return Response(response)

    def post(self, request, *args, **kwargs):
        pass

    def delete(self, request, *args, **kwargs):
        pass
