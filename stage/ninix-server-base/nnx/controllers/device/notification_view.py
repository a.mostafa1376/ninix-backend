from rest_framework import permissions
from rest_framework.exceptions import NotAcceptable
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from nnx.managers.device.notification_manager import NotificationManager
from nnx.serializers.device.notification_shallow_serializer import NotificationShallowSerializer


class NotificationView(GenericAPIView):
    permission_classes = [permissions.AllowAny]
    http_method_names = ['get', 'post']

    def get(self, request, *args, **kwargs):
        manager = NotificationManager()
        notification = manager.get_last_notification()
        response = NotificationShallowSerializer(notification).data
        return Response(response)

    def post(self, request, *args, **kwargs):
        data = request.data
        serializer = NotificationShallowSerializer(data=data)
        valid = serializer.is_valid()
        if not valid:
            raise NotAcceptable(serializer.errors)
        validated_data = serializer.validated_data
        manager = NotificationManager()
        notification = manager.create_notification(validated_data)
        response = NotificationShallowSerializer(notification).data
        return Response(response)

    def delete(self, request, *args, **kwargs):
        pass
