from django import forms
from django.shortcuts import render
from rest_framework import permissions
from rest_framework.exceptions import NotAcceptable
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from nnx.entities.device.boot_loader_model import BootLoader
from nnx.managers.device.boot_loader_manager import BootLoaderManager
from nnx.serializers.device.boot_loader_shallow_serializer import BootLoaderShallowSerializer


class UploadFileForm(forms.Form):
    version_no = forms.CharField(max_length=50)
    version_code = forms.CharField(max_length=50)
    change_log = forms.CharField(max_length=500, required=False)
    boot_loader_file = forms.FileField()


def boot_loader_upload_view(request):
    template = 'upload_boot_loader_template.html'
    context = None
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            boot_loader_file = request.FILES.get('boot_loader_file')
            manager = BootLoaderManager()
            boot_loader_exist, boot_loader_src = manager.save_boot_loader(form.data, boot_loader_file)
            if boot_loader_exist:
                context = {'newer_boot_loader_exist': True}
            else:
                context = {'uploaded_file_url': boot_loader_src}
    return render(request, template, context)


class BootLoaderView(GenericAPIView):
    permission_classes = [permissions.AllowAny]
    http_method_names = ['get']

    @staticmethod
    def _version_code_error():
        error = {
            'version_code': [
                "version code is mandatory."
            ]
        }
        return error

    def get(self, request, *args, **kwargs):
        version_code = request.GET.get(BootLoader.version_code.field_name)
        if not version_code:
            error = self._version_code_error()
            raise NotAcceptable(error)
        manager = BootLoaderManager()
        version_code = int(version_code)
        boot_loader = manager.get_latest_boot_loader(version_code)
        if boot_loader:
            boot_loader_data = BootLoaderShallowSerializer(boot_loader).data
            update_boot_loader = True
        else:
            boot_loader_data = None
            update_boot_loader = False
        response = {
            'availableUpdate': update_boot_loader,
            'bootLoader': boot_loader_data,
        }
        return Response(response)

    def post(self, request, *args, **kwargs):
        pass

    def delete(self, request, *args, **kwargs):
        pass
