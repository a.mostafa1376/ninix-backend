from typing import Dict

from rest_framework.exceptions import NotAcceptable, PermissionDenied
from rest_framework.response import Response
from rest_framework.views import APIView

from nnx.entities.media.image_profile_model import ImageProfile
from nnx.enums.media.image_extension_type import ImageExtensionType
from nnx.managers.media.image_profile_manager import ImageProfileManager
from nnx.managers.media.image_redis_manager import ImageRedisManager
from nnx.serializers.media.image_profile_serializer import ImageProfileSerializer


class ImageView(APIView):
    UPLOAD_SIZE_LIMIT = 20 * 10 ** 6  # ~20 MB
    http_method_names = ['post', 'get']

    def __init__(self):
        super().__init__()
        self.image_profile_manager = ImageProfileManager()
        self.image_redis_manager = ImageRedisManager()

    @staticmethod
    def _err_upload_limit() -> Dict:
        error = {
            'file': [
                "Image size is too large!",
            ]
        }
        return error

    @staticmethod
    def _err(error_msg) -> Dict:
        error = {
            'message': error_msg
        }
        return error

    def handle_upload_image(self, validated_data, content_type, user):
        if not hasattr(user, 'member'):
            raise PermissionDenied()
        else:
            member = user.member
        file = validated_data.get('file')
        object_id = validated_data.get(ImageProfile.object_id.field_name)
        split_file_name = file.name.split('.')
        if len(split_file_name) > 1:
            extension = split_file_name[-1]
            ext = ImageExtensionType.get_value_by_name(extension)
        else:
            ext = None
        file.name = self.image_profile_manager.generate_image_name(ext)
        if file.size > self.UPLOAD_SIZE_LIMIT:
            error = self._err_upload_limit()
            raise NotAcceptable(error)
        else:
            image_profile, status, error_msg = self.image_profile_manager.save_image_profile(
                file=file,
                object_id=object_id,
                member=member,
                ext=ext,
                content_type=content_type
            )
            if image_profile:
                self.image_redis_manager.push_to_queue(file.name)
                response = ImageProfileSerializer(image_profile).data
            else:
                response = self._err(error_msg)
            return Response(response, status=status)
