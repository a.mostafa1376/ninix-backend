from rest_framework import permissions
from rest_framework.response import Response

from nnx.controllers.media.upload_image_view import ImageView
from nnx.enums.media.image_profile_content_type import ImageProfileContentType
from nnx.managers.authorization.member_manager import MemberManager
from nnx.serializers.base_serializer import BaseSerializer
from nnx.serializers.media.image_profile_serializer import ImageProfileDeepSerializer


class MemberImageView(ImageView, BaseSerializer):
    permission_classes = [permissions.IsAuthenticated, ]

    def post(self, request, *args, **kwargs):
        data = request.data
        user = request.user
        validated_data = self.get_valid_data(ImageProfileDeepSerializer, data)
        return self.handle_upload_image(validated_data, ImageProfileContentType.member.value, user)

    def get(self, request, *args, **kwargs):
        object_id = request.GET.get('object_id')
        member_manager = MemberManager()
        image_profile_list = member_manager.get_image_profiles_by_member_id(object_id)
        response = {
            "image_profile_list": []
        }
        for image_profile in image_profile_list:
            image_files_link = self.image_profile_manager.get_image_files_link(image_profile)
            response["image_profile_list"].append(image_files_link)
        return Response(response)
