from rest_framework import permissions
from rest_framework.response import Response

from nnx.controllers.media.upload_image_view import ImageView
from nnx.enums.media.image_profile_content_type import ImageProfileContentType
from nnx.managers.authorization.kid_manager import KidManager
from nnx.managers.media.image_profile_manager import ImageProfileManager
from nnx.serializers.base_serializer import BaseSerializer
from nnx.serializers.media.image_profile_serializer import ImageProfileDeepSerializer


class KidImageView(ImageView, BaseSerializer):
    permission_classes = [permissions.AllowAny, ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.image_profile_manager = ImageProfileManager()

    def post(self, request, *args, **kwargs):
        data = request.data
        user = request.user
        validated_data = self.get_valid_data(ImageProfileDeepSerializer, data)
        return self.handle_upload_image(validated_data, ImageProfileContentType.kid.value, user)

    def get(self, request, *args, **kwargs):
        object_id = request.GET.get('object_id')
        kid_manager = KidManager()
        image_profile_list = kid_manager.get_image_profiles_by_kid_id(object_id)
        response = {
            "image_profile_list": []
        }
        for image_profile in image_profile_list:
            image_files_link = self.image_profile_manager.get_image_files_link(image_profile)
            response["image_profile_list"].append(image_files_link)
        return Response(response)


