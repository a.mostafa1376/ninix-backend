from hs_infra.utils.utils import Utils
from rest_framework import permissions, status
from rest_framework.exceptions import NotAcceptable, PermissionDenied
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from nnx.managers.authorization.authorization_manager import AuthorizationManager


class CheckPasscode(GenericAPIView):
    permission_classes = [permissions.AllowAny]
    http_method_names = ['get', 'post']
    MOBILE_NUMBER_KEY = 'mobile_number'
    PASSCODE_KEY = 'passcode'

    @staticmethod
    def _mobile_number_error():
        error = {
            'mobile_number': [
                "mobile number is not valid"
            ]
        }
        return error

    @staticmethod
    def _passcode_error():
        error = {
            'passcode': [
                "passcode is mandatory"
            ]
        }
        return error

    @staticmethod
    def _auth_error():
        error = {
            'authorization': [
                "Authorization credential is not valid"
            ]
        }
        return error

    @staticmethod
    def _manage_auth(mobile_number, passcode, request):
        auth_manager = AuthorizationManager()
        is_valid, error_msg = auth_manager.check_passcode(mobile_number, passcode)
        if is_valid:
            status_code = status.HTTP_200_OK
            message = 'success'
            token = auth_manager.authenticate_user(mobile_number, request)
        else:
            raise PermissionDenied(error_msg)
        response = {
            'status_code': status_code,
            'message': message,
            'token': token,
        }
        return response

    def _validate_mobile_number(self, raw_mobile_number):
        try:
            mobile_number = Utils.normalize_mobile_number(raw_mobile_number)
        except ValueError:
            error = self._mobile_number_error()
            raise NotAcceptable(error)
        except AttributeError:
            error = self._mobile_number_error()
            raise NotAcceptable(error)
        return mobile_number
    
    @staticmethod
    def is_number(s):
        try:
            int(s)
            return True
        except ValueError:
            return False
    def get(self, request, *args, **kwargs):
        raw_mobile_number = request.GET.get(self.MOBILE_NUMBER_KEY)
        if self.is_number(raw_mobile_number):
            mobile_number = self._validate_mobile_number(raw_mobile_number)
        else:
            mobile_number = raw_mobile_number
        passcode = request.GET.get(self.PASSCODE_KEY)
        response = self._manage_auth(mobile_number, passcode, request)
        return Response(response)

    def post(self, request, *args, **kwargs):
        raw_mobile_number = request.data.get(self.MOBILE_NUMBER_KEY)
        mobile_number = self._validate_mobile_number(raw_mobile_number)
        passcode = request.data.get(self.PASSCODE_KEY)
        if passcode is None:
            error = self._passcode_error()
            raise NotAcceptable(error)
        response = self._manage_auth(mobile_number, passcode, request)
        return Response(response)
