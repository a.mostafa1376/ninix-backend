from hs_infra.utils.utils import Utils
from rest_framework import permissions, status
from rest_framework.exceptions import NotAcceptable, PermissionDenied
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from nnx.managers.authorization.authorization_manager import AuthorizationManager


class GeneratePasscode(GenericAPIView):
    permission_classes = [permissions.AllowAny]
    http_method_names = ['get']
    MOBILE_NUMBER_KEY = 'mobile_number'

    @staticmethod
    def _mobile_number_error():
        error = {
            'mobile_number': [
                "mobile number is not valid"
            ]
        }
        return error

    def get(self, request, *args, **kwargs):
        #raw_mobile_number = request.GET.get(self.MOBILE_NUMBER_KEY)
        mobile_number = request.GET.get(self.MOBILE_NUMBER_KEY)
#         if not raw_mobile_number:
#             error = self._mobile_number_error()
#             raise NotAcceptable(error)
#         try:
#             mobile_number = Utils.normalize_mobile_number(raw_mobile_number)
#         except ValueError:
#             error = self._mobile_number_error()
#             raise NotAcceptable(error)
        auth_manager = AuthorizationManager()
        generated, error = auth_manager.generate_passcode(mobile_number)
        registered = auth_manager.is_registered(mobile_number)
        if not generated:
            raise PermissionDenied(error)

        response = {
            'status_code': status.HTTP_200_OK,
            'is_registered': registered,
            'message': 'success',
        }
        return Response(response)
