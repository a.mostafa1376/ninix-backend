from rest_framework import permissions
from rest_framework.exceptions import NotAcceptable, MethodNotAllowed, NotFound
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from ninix.config.runtime_config import RuntimeConfig
from nnx.controllers.authorization.member_view import MemberView
from nnx.entities.authorization.kid_model import Kid
from nnx.managers.authorization.kid_manager import KidManager
from nnx.serializers.authorizations.kid_shallow_serializer import KidShallowSerializer

from nnx.managers.log.tracking_log_manager import TrackingLogManager
from hs_infra.utils.utils import Utils

class KidView(GenericAPIView):
    permission_classes = [permissions.IsAuthenticated]
    http_method_names = ['get', 'post', 'delete']

    @staticmethod
    def err_no_kid_found():
        error = {
            'kid': [
                "No kid registered for this device.",
            ]
        }
        return error

    @staticmethod
    def device_uid_is_active(device_uid):
        manager = TrackingLogManager()
        track_log = manager.find_log_record(device_uid)
        
        if len(track_log) == 0:
            return False
        
        track_log.pop('meta_data')
        
        from numpy import array, concatenate
        timestamps = array([])
        for key in track_log.keys():
            timestamps = concatenate((timestamps,list(track_log[key].keys())))
         
        timestamps = [int(x) for x in timestamps]
        now = int(Utils.get_current_datetime().timestamp())
        distance = now - max(timestamps)
        print(distance)
        if distance < 60:
            return True
        
        return False
    
    def get(self, request, *args, **kwargs):
        device_uid = request.GET.get(Kid.device_uid.field_name)
        user = request.user
        if not hasattr(user, 'member'):
            error = MemberView.err_no_member_found()
            raise NotFound(error)
        
        member = user.member
        manager = KidManager()
        
        if len(device_uid) == 0:
            kids = member.kids.all()
            response = dict()
            for k in kids:
                kid = k
                device_uid = kid.device_uid
                response[device_uid] = self.device_uid_is_active(device_uid)
            if kid:
                return Response(response)
        else:
            kid = manager.get_kid_by_device_uid(device_uid, member)
        
        if kid:
            response = KidShallowSerializer(kid).data
        else:
            error = self.err_no_kid_found()
            raise NotFound(error)
        return Response(response)

    def post(self, request, *args, **kwargs):
        
        ####################
        device_uid = request.data.get('device_uid')
        
        ####################
        
        user = request.user
        if not hasattr(user, 'member'):
            error = MemberView.err_no_member_found()
            raise NotFound(error)
        member = user.member
        serializer = KidShallowSerializer(data=request.data)
        valid = serializer.is_valid()
        if not valid:
            raise NotAcceptable(serializer.errors)
        validated_data = serializer.validated_data
        kid_manager = KidManager()
        kid = kid_manager.create_kid(validated_data, member)
        response = KidShallowSerializer(kid).data
        return Response(response)

    def delete(self, request, *args, **kwargs):
        if RuntimeConfig.DEVELOPER_MODE is False:
            raise MethodNotAllowed('Allowed only in developer mode.')
        device_uid = request.GET.get(Kid.device_uid.field_name)
        user = request.user
        if not hasattr(user, 'member'):
            error = MemberView.err_no_member_found()
            raise NotFound(error)
        member = user.member
        manager = KidManager()
        kid = manager.get_kid_by_device_uid(device_uid, member)
        if kid:
            kid.members.clear()
            kid.delete()
            response = {
                'message': 'deleted'
            }
        else:
            error = self.err_no_kid_found()
            raise NotFound(error)
        return Response(response)