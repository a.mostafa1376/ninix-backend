from rest_framework import permissions
from rest_framework.exceptions import NotAcceptable, MethodNotAllowed, NotFound
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from ninix.config.runtime_config import RuntimeConfig
from nnx.entities.authorization.member_model import Member
from nnx.managers.authorization.member_manager import MemberManager
from nnx.serializers.authorizations.member_shallow_serializer import MemberShallowSerializer


class MemberView(GenericAPIView):
    permission_classes = [permissions.IsAuthenticated]
    http_method_names = ['get', 'post', 'delete']

    @staticmethod
    def err_no_member_found():
        error = {
            'member': [
                "Member not registered.",
            ]
        }
        return error

    def get(self, request, *args, **kwargs):
        user = request.user
        if hasattr(user, 'member'):
            response = MemberShallowSerializer(user.member).data
        else:
            error = self.err_no_member_found()
            raise NotFound(error)
        return Response(response)

    def post(self, request, *args, **kwargs):
        user = request.user
        data = request.data
        data[Member.mobile_number.field_name] = user.username
        serializer = MemberShallowSerializer(data=data)
        valid = serializer.is_valid()
        if not valid:
            raise NotAcceptable(serializer.errors)
        validated_data = serializer.validated_data
        member_manager = MemberManager()
        member = member_manager.create_member(validated_data)
        response = MemberShallowSerializer(member).data
        return Response(response)

    def delete(self, request, *args, **kwargs):
        if RuntimeConfig.DEVELOPER_MODE is False:
            raise MethodNotAllowed('Allowed only in developer mode.')
        user = request.user
        if not hasattr(user, 'member'):
            error = self._err_has_no_member()
            raise NotFound(error)
        member = user.member
        member.kids.clear()
        member.delete()
        response = {
            'message': 'deleted'
        }
        return Response(response)
