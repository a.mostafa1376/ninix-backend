from rest_framework import permissions
from rest_framework.exceptions import NotAcceptable
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from nnx.managers.authorization.member_manager import MemberManager
from nnx.serializers.authorizations.kid_shallow_serializer import KidShallowSerializer
from nnx.managers.log.member_logs_manager import MemberLogsManager

class MemberLogsView(GenericAPIView):
    permission_classes = [permissions.IsAuthenticated]
    http_method_names = ['get']
    
#     def get(self, request, *args, **kwargs):
#         data = request.GET
#         collection = data.get('data_mode')
        
#         device_uid = data.get(self.DEVICE_UID_KEY)
#         if device_uid is None:
#             error = self._err_device_uid()
#             raise NotAcceptable(error)
#         manager = TrackingLogManager()
#         track_log = manager.find_log_record(device_uid, data, collection)
        
#         return Response(track_log)   
        
        
    def get(self, request, *args, **kwargs):
        data = request.GET
        member_id = int(data.get('member_id'))
        member = MemberManager().find_member_by_id(member_id)
        kids = member.kids.all()

        manager = MemberLogsManager()        
        response = []
        for kid in kids:
            kid_data = KidShallowSerializer(kid).data
            kid_data['logs'] = manager.find_logs(kid_data['id'])
            response.append(kid_data)

        return Response(response)
