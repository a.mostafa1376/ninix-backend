from rest_framework import permissions
from rest_framework.exceptions import NotAcceptable
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from nnx.managers.log.tracking_log_manager import TrackingLogManager
from nnx.serializers.log.tracking_log_shallow_serializer import TrackingLogShallowSerializer


class TrackingLogView(GenericAPIView):
    permission_classes = [permissions.IsAuthenticated]
    http_method_names = ['get', 'post']
    DEVICE_UID_KEY = 'device_uid'

    @staticmethod
    def _err_device_uid():
        key = TrackingLogView.DEVICE_UID_KEY
        error = {
            'error_code': 405,
            key: [
                f"{key} is mandatory.",
            ]
        }
        return error
    
    @staticmethod
    def _err_device_uid_kid():
        key = TrackingLogView.DEVICE_UID_KEY
        error = {
            'error_code': 406,
            key: [
                "Kid with this device_uid not found.",
            ]
        }
        return error

    @staticmethod
    def convert_response(dictionary):
        from numpy import array, concatenate, unique
        timestamps = array([])
        for key in dictionary.keys():
            timestamps = concatenate((timestamps,list(dictionary[key].keys())))
            
        timestamps = unique(timestamps)
        response = []
        for timestamp in timestamps:
            d = dict()
            d['timestamp'] = timestamp
            for key in dictionary.keys():
                if timestamp in dictionary[key].keys():
                    d[key] = dictionary[key][timestamp]
            response.append(d)
        return response

#     def get(self, request, *args, **kwargs):
#         from pandas import DataFrame
#         from ast import literal_eval
#         data = request.data
#         collection = data.get('collection')
#         device_uid = data.get(self.DEVICE_UID_KEY)
#         if device_uid is None:
#             error = self._err_device_uid()
#             raise NotAcceptable(error)
#         manager = TrackingLogManager()
#         track_log = manager.find_log_record(device_uid, collection)
#         track_log.pop('meta_data')
#         # is track_log None only when kid is none?
#         if track_log is None:
#             error = self._err_device_uid_kid()
#             raise NotAcceptable(error)
        
#         default_data = {
#           "device_uid": device_uid, 
#           "all_data": "True",
#           "identifier": "",
#           "from":0,
#           "until": 10000000000
#         }
        
#         for key in default_data.keys():
#             if not (key in data.keys()):
#                 data[key] = default_data[key]
                
#         if data.get('all_data') == 'True':
#             return Response(self.convert_response(track_log))
#         else:
#             logs = literal_eval(data.get('identifier'))
#             response = dict()
#             for l in logs:
#                 log = track_log[l]
#                 df = DataFrame(log.items())
#                 df[0] = df[0].astype(int)
#                 df = df[(df[0] >= data.get('from')) & (df[0] <= data.get('until'))]
#                 t_log = dict()
#                 for i in range(len(df)):
#                     element = df.iloc[i]
#                     t_log[int(element[0])] = element[1]
#                 response[l] = t_log
            
#             response = self.convert_response(response)
#             return Response(response)
   
    
    
    def get(self, request, *args, **kwargs):
        data = request.GET
        collection = data.get('data_mode')
        
        device_uid = data.get(self.DEVICE_UID_KEY)
        if device_uid is None:
            error = self._err_device_uid()
            raise NotAcceptable(error)
        manager = TrackingLogManager()
        track_log = manager.find_log_record(device_uid, data, collection)
        
        return Response(track_log)   
        
        
    def post(self, request, *args, **kwargs):
        #same timestamp should not send
        data = request.GET
        collection = data.get('data_mode')
        overwrite = data.get('overwrite')
        serializer = TrackingLogShallowSerializer(data=data)
        user = request.user
        member = user.member
        valid = serializer.is_valid()
        if not valid:
            raise NotAcceptable(serializer.errors)
        validated_data = serializer.validated_data
        device_uid = data.get(self.DEVICE_UID_KEY)
        manager = TrackingLogManager()
        print(validated_data)
        tracking = manager.create_log_record(member, device_uid, validated_data, overwrite, collection)
        response = TrackingLogShallowSerializer(tracking).data
        return Response(response)
