from rest_framework import permissions
from rest_framework.exceptions import NotAcceptable
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from nnx.managers.log.active_devices_manager import ActiveDevicesManager
from nnx.serializers.authorizations.kid_shallow_serializer import KidShallowSerializer

class ActiveDevicesView(GenericAPIView):
    permission_classes = [permissions.IsAuthenticated]
    http_method_names = ['post']
    
#     def get(self, request, *args, **kwargs):
#         data = request.GET
#         collection = data.get('data_mode')
        
#         device_uid = data.get(self.DEVICE_UID_KEY)
#         if device_uid is None:
#             error = self._err_device_uid()
#             raise NotAcceptable(error)
#         manager = TrackingLogManager()
#         track_log = manager.find_log_record(device_uid, data, collection)
        
#         return Response(track_log)   
        
        
    def post(self, request, *args, **kwargs):
        data = request.data
        collection = data.get('collection')
        from_timestamp = int(data.get('from'))
        until_timestamp = int(data.get('until'))
        manager = ActiveDevicesManager()
        active_devices = manager.find_active_devices(from_timestamp, until_timestamp, collection)
        active_devices = [KidShallowSerializer(kid).data for kid in active_devices]
        return Response(active_devices)
