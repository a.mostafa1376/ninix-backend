from rest_framework import permissions
from rest_framework.exceptions import NotAcceptable
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from nnx.managers.log.raw_log_manager import RawLogManager
from nnx.serializers.log.raw_log_shallow_serializer import RawLogShallowSerializer


class RawLogView(GenericAPIView):
    permission_classes = [permissions.IsAuthenticated]
    http_method_names = ['post']

    def post(self, request, *args, **kwargs):
        data = request.data
        serializer = RawLogShallowSerializer(data=data)
        user = request.user
        member = user.member
        valid = serializer.is_valid()
        if not valid:
            raise NotAcceptable(serializer.errors)
        validated_data = serializer.validated_data
        manager = RawLogManager()
        raw_log = manager.create_log_record(member, validated_data)
        response = RawLogShallowSerializer(raw_log).data
        return Response(response)

