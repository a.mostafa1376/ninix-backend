from django.db import models
from hs_infra.entities.base.base_entity import BaseModel

from nnx.entities.media.image_profile_model import ImageProfile
from nnx.enums.media.common_aspect_ratios import CommonAspectRatios


class ImageFile(BaseModel):
    file = models.ImageField()
    file_name = models.CharField(max_length=255)
    ext = models.CharField(max_length=10)
    aspect_ratio = models.IntegerField(
        choices=CommonAspectRatios.choices(),
        default=CommonAspectRatios.unknown
    )
    width = models.IntegerField()
    height = models.IntegerField()

    image_profile = models.ForeignKey(
        to=ImageProfile,
        related_name='image_files',
        related_query_name='image_file',
        on_delete=models.CASCADE
    )
