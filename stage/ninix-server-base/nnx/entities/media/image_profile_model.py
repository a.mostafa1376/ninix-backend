from django.contrib.contenttypes import fields
from django.contrib.contenttypes.models import ContentType
from django.db import models
from hs_infra.entities.base.base_entity import BaseModel

from nnx.enums.media.image_extension_type import ImageExtensionType


class ImageProfile(BaseModel):
    file = models.ImageField(upload_to='image_profile')
    file_name = models.CharField(max_length=255)
    extension = models.IntegerField(
        choices=ImageExtensionType.choices(),
        default=ImageExtensionType.unknown,
    )

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = fields.GenericForeignKey()
