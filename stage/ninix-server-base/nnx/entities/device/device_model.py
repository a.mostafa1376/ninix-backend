from django.db import models
from hs_infra.entities.base.base_entity import BaseModel


class Device(BaseModel):
    issue_number = models.IntegerField(blank=False, null=False, unique=False)
    production_series = models.IntegerField(default=2)

    @property
    def serial_code(self):
        production_str = "{0:0=3d}".format(self.production_series)
        issue_str = "{0:0=6d}".format(self.issue_number)
        time_str = self.created_at.strftime("%Y%m%d")
        serial_code = time_str + production_str + issue_str
        return serial_code
