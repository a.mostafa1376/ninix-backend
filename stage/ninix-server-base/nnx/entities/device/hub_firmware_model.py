from django.db import models
from hs_infra.entities.base.base_entity import BaseModel


class HubFirmware(BaseModel):
    version_no = models.CharField(max_length=10, blank=False, null=False, unique=True)
    version_code = models.IntegerField(blank=False, null=False, unique=True)
    change_log = models.CharField(max_length=500, blank=True, null=True)
    hub_firmware_src = models.CharField(max_length=200, blank=True, null=True)
