from django.db import models
from hs_infra.entities.base.base_entity import BaseModel


class Notification(BaseModel):
    serial_number = models.IntegerField(blank=False, null=False, unique=False)
    notification_type = models.IntegerField(blank=False, null=False, unique=False)
    timestamp = models.IntegerField(null=False, blank=False, unique=False)
