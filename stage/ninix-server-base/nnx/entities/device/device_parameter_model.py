from django.db import models
from hs_infra.entities.base.base_entity import BaseModel


class DeviceParameter(BaseModel):
    parameter_name = models.CharField(max_length=50, blank=False, null=False, unique=False)
    parameter_detail = models.CharField(max_length=500, blank=True, null=True, default=None)
    parameter_value = models.FloatField(default=0, unique=False)
    parameter_formula = models.CharField(max_length=500, unique=False, null=False, default="v")
