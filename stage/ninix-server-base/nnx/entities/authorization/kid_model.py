from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from hs_infra.entities.base.base_entity import BaseModel

from nnx.entities.authorization.member_model import Member
from nnx.entities.media.image_profile_model import ImageProfile
from nnx.enums.authorization.gender_type import GenderType


class Kid(BaseModel):
    first_name = models.CharField(max_length=100, blank=True, null=True)
    last_name = models.CharField(max_length=100, blank=True, null=True)
    birth_date = models.DateTimeField()
    device_uid = models.CharField(max_length=20, blank=False, null=False, unique=True)

    # enums
    gender = models.IntegerField(
        choices=GenderType.choices(),
        null=False,
        blank=False,
    )

    # related models
    members = models.ManyToManyField(
        to=Member,
        blank=False,
        related_name='kids',
        related_query_name='kid',
    )
    image_profile = GenericRelation(ImageProfile)
