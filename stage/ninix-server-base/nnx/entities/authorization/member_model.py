from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from hs_infra.entities.base.base_entity import BaseModel

from nnx.entities.media.image_profile_model import ImageProfile
from nnx.enums.authorization.active_status_type import ActiveStatusType
from nnx.enums.authorization.relationship_type import RelationshipType


class Member(BaseModel):
    first_name = models.CharField(max_length=100, blank=True, null=True)
    last_name = models.CharField(max_length=100, blank=True, null=True)
    mobile_number = models.CharField(max_length=100, blank=False, null=False, unique=True)
    birth_year = models.IntegerField(blank=False, null=False)

    # enums
    active_status_type = models.IntegerField(
        choices=ActiveStatusType.choices(),
        default=ActiveStatusType.active.value,
    )
    relationship_type = models.IntegerField(
        choices=RelationshipType.choices(),
        default=RelationshipType.mother.value,
    )

    # related models
    user = models.OneToOneField(
        to=User,
        on_delete=models.CASCADE,
        blank=False,
        unique=True,
        related_name='member',
        related_query_name='member',
    )
    image_profile = GenericRelation(ImageProfile)
