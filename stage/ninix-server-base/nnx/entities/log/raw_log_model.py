from django.db import models
from hs_infra.entities.base.base_entity import BaseModel

from nnx.enums.log.log_level_type import LogLevelType

from nnx.entities.authorization.kid_model import Kid
from nnx.entities.authorization.member_model import Member


class RawLog(BaseModel):
    log_message = models.CharField(max_length=100, blank=False, null=False)
    log_level = models.IntegerField(choices=LogLevelType.choices(), blank=False, null=False)
    device_uid = models.CharField(max_length=20, blank=False, null=False, unique=False, default="0"*20)
    log_data = models.CharField(max_length=500, blank=True, null=True)
    ####added
#     kid = models.ForeignKey(
#         to=Kid,
#         on_delete=models.CASCADE,
#         related_name='raw_logs',
#         related_query_name='raw_log',
#         default=None,
#     )
#     member = models.ForeignKey(
#         to=Member,
#         on_delete=models.CASCADE,
#         related_name='raw_logs',
#         related_query_name='raw_log',
#         default=None,
#     )

# You are trying to add a non-nullable field 'kid' to rawlog without a default; we can't do that (the database needs something to populate existing rows).
# Please select a fix:
#  1) Provide a one-off default now (will be set on all existing rows with a null value for this column)
#  2) Quit, and let me add a default in models.py
# Select an option: 1
# Please enter the default value now, as valid Python
# The datetime and django.utils.timezone modules are available, so you can do e.g. timezone.now
# Type 'exit' to exit this prompt
# >>> 0
# You are trying to add a non-nullable field 'member' to rawlog without a default; we can't do that (the database needs something to populate existing rows).
# Please select a fix:
#  1) Provide a one-off default now (will be set on all existing rows with a null value for this column)
#  2) Quit, and let me add a default in models.py
# Select an option: 1
# Please enter the default value now, as valid Python
# The datetime and django.utils.timezone modules are available, so you can do e.g. timezone.now
# Type 'exit' to exit this prompt
# >>> 0
# Migrations for 'nnx':
#   nnx/migrations/0015_auto_20200310_1159.py
#     - Add field kid to rawlog
#     - Add field member to rawlog
