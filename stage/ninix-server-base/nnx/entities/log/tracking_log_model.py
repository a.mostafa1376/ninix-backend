from django.db import models
from hs_infra.entities.base.base_entity import BaseModel

from nnx.entities.authorization.kid_model import Kid
from nnx.entities.authorization.member_model import Member


class TrackingLog(BaseModel):
    log_data = models.CharField(max_length=500, blank=True, null=True)
    timestamp = models.CharField(max_length=10, blank=False, null=False, default="0000000000")

    kid = models.ForeignKey(
        to=Kid,
        on_delete=models.CASCADE,
        related_name='tracking_logs',
        related_query_name='tracking_log',
    )
    member = models.ForeignKey(
        to=Member,
        on_delete=models.CASCADE,
        related_name='tracking_logs',
        related_query_name='tracking_log',
    )
